use wasm_bindgen::prelude::*;

// All code are moved into the `share-lib` for sharing with the demos in the `/guide`.
// The guide is at: https://limira-rs.gitlab.io/mika/
//
// The component Fetch is defined in `/examples/share-lib/src/fetch`
// We just import it here and create an app handle for it.
mika::create_app_handle!(share_lib::fetch::FetchComp);
