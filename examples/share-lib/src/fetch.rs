use mika::prelude::*;
use serde::Deserialize;
use signals::signal::SignalExt;

#[derive(Clone, Deserialize)]
struct Commit {
    id: String,
    short_id: String,
    title: String,
    created_at: String,
    message: String,
    author_name: String,
    author_email: String,
    authored_date: String,
}

#[derive(Clone, Deserialize)]
pub struct Branch {
    name: String,
    commit: Commit,
}

pub struct Fetch {
    message: Option<String>,
    master_branch_info: Option<Branch>,
}

impl Default for Fetch {
    fn default() -> Self {
        Self {
            message: Some("Please click the button".to_string()),
            master_branch_info: None,
        }
    }
}

impl Fetch {
    fn start_fetching(&mut self) -> mika::Context<FetchComp> {
        let mut context = mika::Context::default();
        let url = "https://gitlab.com/api/v4/projects/12457005/repository/branches/master";
        let req = mika::fetch::Request::new(mika::fetch::Method::Get, url);
        context
            .commands
            .fetch_json_ok_error2(req, Fetch::set_data, Fetch::set_error);
        self.message = Some("A fetch request is sent. Please wait for a few seconds".to_string());
        context
    }

    fn set_data(&mut self, data: Branch) {
        self.master_branch_info = Some(data);
        self.message = None;
    }

    fn set_error(&mut self, e: mika::Error) {
        self.message = Some(e.to_string())
    }
}

#[derive(Default)]
pub struct FetchComp {
    message: signals::signal::Mutable<Option<String>>,
    master_branch_info: signals::signal::Mutable<Option<Branch>>,
}

impl mika::Component for FetchComp {
    type State = Fetch;
    type ListChange = ();
    type Output = mika::dom::Div;

    fn pre_render(_: &mika::CompHandle<Self>) -> Option<Vec<Box<dyn mika::events::Listener>>> {
        wasm_logger::init(wasm_logger::Config::new(log::Level::Debug).message_on_new_line());
        None
    }

    fn diff(&self, data: &Fetch, _: ()) {
        self.message.set_neq(data.message.clone());
        self.master_branch_info.set(data.master_branch_info.clone());
    }

    fn render(&self, comp: &mika::CompHandle<Self>) -> Self::Output {
        mika::dom::Div::new()
            .child(mika::dom::H1::new().text("Mika latest commit info"))
            .child_signal(self.message.signal_cloned().map({
                let comp = comp.clone();
                move |value| match value {
                    None => mika::dom::Div::new()
                        .child(mika::dom::P::new().text("Fetched successfully")),
                    Some(text) => mika::dom::Div::new()
                        .child(mika::dom::P::new().text(&text))
                        .child(
                            mika::dom::Button::new()
                                .text("Fetch Mika latest commit info")
                                .on_click(
                                    mika::handler! { &comp, Fetch::start_fetching },
                                ),
                        ),
                }
            }))
            .child_signal(self.master_branch_info.signal_cloned().map(move |value| {
                match value {
                    None => mika::dom::Table::new(),
                    Some(data) => mika::dom::Table::new()
                        .child(tr("Branch", &data.name))
                        .child(tr("Commit id", &data.commit.id))
                        .child(tr("Title", &data.commit.title))
                        .child(tr("Created at", &data.commit.created_at))
                        .child(tr("Message", &data.commit.message))
                        .child(tr("Author name", &data.commit.author_name))
                        .child(tr("Authored date", &data.commit.authored_date))
                        .into(),
                }
            }))
            .into()
    }
}

fn tr(title: &str, value: &str) -> mika::dom::Tr {
    mika::dom::Tr::new()
        .child(mika::dom::Th::new().text(title))
        .child(mika::dom::Td::new().text(value))
        .into()
}
