#[derive(Copy, Clone, PartialEq)]
pub enum CounterMode {
    SingleStep,
    MultiSteps,
}

impl Default for CounterMode {
    fn default() -> Self {
        CounterMode::SingleStep
    }
}

impl crate::stateless_view_renders::RadioItem for CounterMode {
    fn text(&self) -> &'static str {
        match self {
            CounterMode::SingleStep => "Normal counter",
            CounterMode::MultiSteps => "Multi-steps counter",
        }
    }
    fn value(&self) -> &'static str {
        self.text()
    }
}

pub struct Counters {
    pub mode: CounterMode,
    pub year: i32,
    pub month: i32,
}

impl Default for Counters {
    fn default() -> Self {
        Self {
            mode: CounterMode::SingleStep,
            year: 2019,
            month: 7,
        }
    }
}

impl Counters {
    pub fn year_down(&mut self) {
        self.year -= 1;
    }

    pub fn year_up(&mut self) {
        self.year += 1;
    }

    pub fn month_down(&mut self, step: i32) {
        self.month -= step;
        if self.month < 1 {
            self.month += 12;
            self.year -= 1;
        }
    }

    pub fn month_up(&mut self, step: i32) {
        self.month += step;
        if self.month > 12 {
            self.month -= 12;
            self.year += 1;
        }
    }

    pub fn set_mode(&mut self, mode: CounterMode) {
        self.mode = mode
    }
}
