// Pieces of code that are called by renders

use mika::prelude::*;

// You may split your code into functions
pub fn counter_view<S, T, F1, F2>(title: &str, signal: S, up: F1, down: F2) -> mika::dom::Div
where
    S: 'static + signals::signal::Signal<Item = T>,
    T: 'static + ToString,
    F1: mika::events::Click,
    F2: mika::events::Click,
{
    mika::dom::Div::new()
        .child(mika::dom::H2::new().text(title))
        .child(mika::dom::Button::new().text("Down").on_click(down))
        .text(" ")
        .text_signal(signal)
        .text(" ")
        .child(mika::dom::Button::new().text("Up").on_click(up))
        .into()
}

// Or you may split your code into little structs that impls a method that render the view.
pub struct CounterView<'a, S, F1, F2> {
    pub title: &'a str,
    pub signal: S,
    pub up: F1,
    pub down: F2,
}

impl<'a, S, T, F1, F2> CounterView<'a, S, F1, F2>
where
    S: 'static + signals::signal::Signal<Item = T>,
    T: 'static + ToString,
    F1: mika::events::Click,
    F2: mika::events::Click,
{
    pub fn render(self) -> mika::dom::Div {
        mika::dom::Div::new()
            .child(mika::dom::H2::new().text(self.title))
            .child(mika::dom::Button::new().text("Down").on_click(self.down))
            .text(" ")
            .text_signal(self.signal)
            .text(" ")
            .child(mika::dom::Button::new().text("Up").on_click(self.up))
            .into()
    }
}

pub struct MultiStepCounterView<'a, S, Fu5, Fu1, Fd1, Fd5> {
    pub title: &'a str,
    pub signal: S,
    pub up_5: Fu5,
    pub up_1: Fu1,
    pub down_1: Fd1,
    pub down_5: Fd5,
}

impl<'a, S, T, Fu5, Fu1, Fd1, Fd5> MultiStepCounterView<'a, S, Fu5, Fu1, Fd1, Fd5>
where
    S: 'static + signals::signal::Signal<Item = T>,
    T: 'static + ToString,
    Fu5: mika::events::Click,
    Fu1: mika::events::Click,
    Fd1: mika::events::Click,
    Fd5: mika::events::Click,
{
    pub fn render(self) -> mika::dom::Div {
        mika::dom::Div::new()
            .child(mika::dom::H2::new().text(self.title))
            .child(mika::dom::Button::new().text("-5").on_click(self.down_5))
            .child(mika::dom::Button::new().text("-1").on_click(self.down_1))
            .text(" ")
            .text_signal(self.signal)
            .text(" ")
            .child(mika::dom::Button::new().text("+1").on_click(self.up_1))
            .child(mika::dom::Button::new().text("+5").on_click(self.up_5))
            .into()
    }
}

pub trait RadioItem: Copy + PartialEq {
    fn text(&self) -> &'static str;
    fn value(&self) -> &'static str;
}

pub struct ModeList {
    container: mika::dom::FieldSet,
    radio_name: &'static str,
}

impl ModeList {
    pub fn new(radio_name: &'static str, legend: &str) -> Self {
        Self {
            container: mika::dom::FieldSet::new()
                .child(mika::dom::Legend::new().text(legend))
                .into(),
            radio_name,
        }
    }
    pub fn add<I, S, C, F>(
        mut self,
        item: I,
        checked_signal: S,
        comp_handle: &mika::CompHandle<C>,
        f: F,
    ) -> Self
    where
        I: 'static + RadioItem,
        S: 'static + signals::signal::Signal<Item = I>,
        C: 'static + mika::Component,
        F: 'static + Fn(&mut C::State, I),
    {
        use signals::signal::SignalExt;

        // Because the `.child()` method moved the `container`, so we need to assign it back.
        self.container = self
            .container
            .child(
                mika::dom::Label::new()
                    .child(
                        mika::dom::Input::new()
                            .r#type(mika::dom::InputType::RadioButton)
                            .name(self.radio_name)
                            .checked_signal(checked_signal.map(move |current| current == item))
                            .value(item.value())
                            .on_change(mika::handler!(comp_handle, f(item))),
                    )
                    .text(item.text()),
            )
            .child(mika::dom::Br::new())
            .into();
        self
    }
    pub fn done(self) -> mika::dom::FieldSet {
        self.container
    }
}
