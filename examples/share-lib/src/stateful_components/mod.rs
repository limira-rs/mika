use mika::prelude::*;
use signals::signal::{Mutable, SignalExt};

mod counter;
mod send_back_counter;

#[derive(Clone, Copy, PartialEq)]
pub enum CounterMode {
    SendBackCounter,
    NormalCounter,
}

impl Default for CounterMode {
    fn default() -> Self {
        CounterMode::SendBackCounter
    }
}

impl crate::stateless_view_renders::RadioItem for CounterMode {
    fn text(&self) -> &'static str {
        match self {
            CounterMode::SendBackCounter => "Send back counter",
            CounterMode::NormalCounter => "Normal counter",
        }
    }
    fn value(&self) -> &'static str {
        self.text()
    }
}

pub struct MainState {
    mode: CounterMode,
    value: i32,
}

impl send_back_counter::ValueReceiver for MainState {
    fn receive_value_from_child_component(&mut self, value: i32) {
        self.value = value;
    }
}

impl Default for MainState {
    fn default() -> Self {
        Self {
            mode: CounterMode::SendBackCounter,
            value: 42,
        }
    }
}

impl MainState {
    fn set_mode(&mut self, mode: CounterMode) {
        self.mode = mode;
    }

    fn up(&mut self) {
        self.value += 1;
    }

    fn down(&mut self) {
        self.value -= 1;
    }
}

#[derive(Default)]
pub struct MainComp {
    mode: Mutable<CounterMode>,
    value: Mutable<i32>,
}

impl mika::Component for MainComp {
    type State = MainState;
    type ListChange = ();
    type Output = mika::dom::Div;

    fn pre_render(_: &mika::CompHandle<Self>) -> Option<Vec<Box<dyn mika::events::Listener>>> {
        wasm_logger::init(wasm_logger::Config::new(log::Level::Debug).message_on_new_line());
        None
    }

    fn diff(&self, data: &MainState, _: ()) {
        self.mode.set_neq(data.mode.clone());
        self.value.set_neq(data.value);
    }

    fn render(&self, comp: &mika::CompHandle<Self>) -> Self::Output {
        mika::dom::Div::new()
            .child(mika::dom::H2::new().text("Counters"))
            .child(
                mika::dom::P::new().text(
                    "Demonstrate for how to add stateful components and communicate with them",
                ),
            )
            .child(mika::dom::Hr::new())
            .child(
                crate::stateless_view_renders::ModeList::new("mode", "Select a sub component mode")
                    .add(
                        CounterMode::SendBackCounter,
                        self.mode.signal(),
                        comp,
                        MainState::set_mode,
                    )
                    .add(
                        CounterMode::NormalCounter,
                        self.mode.signal(),
                        comp,
                        MainState::set_mode,
                    )
                    .done(),
            )
            .child(
                crate::stateless_view_renders::CounterView {
                    title: "Value of the main component",
                    signal: self.value.signal(),
                    up: mika::handler!(comp, MainState::up),
                    down: mika::handler!(comp, MainState::down),
                }
                .render(),
            )
            .child(mika::dom::P::new().text(
                "Note that the value above will be sent to the 'SendBackCounter' on each change.",
            ))
            .child(
                mika::dom::FieldSet::new()
                    .child(mika::dom::Legend::new().text("Active component"))
                    .component_signal(self.mode.signal_cloned().map({
                        let value = self.value.clone();
                        let comp1 = comp.clone();
                        move |mode| match mode {
                            CounterMode::SendBackCounter => {
                                let mut cc = send_back_counter::SendBackCounterComp::new(
                                    send_back_counter::SendBackCounter::new(
                                        value.get(),
                                        comp1.clone(),
                                    ),
                                );
                                cc.update_signal(value.signal().map(|value| {
                                    move |sbc: &mut send_back_counter::SendBackCounter<MainComp>| {
                                        sbc.set_value_from_parent(value);
                                    }
                                }));
                                cc.into()
                            }
                            CounterMode::NormalCounter => {
                                counter::CounterComp::new(counter::Counter::new(value.get(), 3))
                                    .into()
                            }
                        }
                    })),
            )
            .into()
    }
}
