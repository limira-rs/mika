use signals::signal::Mutable;

pub struct Counter {
    pub value: i32,
    pub step: i32,
}

impl Counter {
    pub fn new(value: i32, step: i32) -> Self {
        Self { value, step }
    }

    fn up(&mut self) {
        self.value += self.step;
    }

    fn down(&mut self) {
        self.value -= self.step;
    }
}

impl Drop for Counter {
    fn drop(&mut self) {
        log::info!("Dropping Counter with step={}", self.step);
    }
}

#[derive(Default)]
pub struct CounterComp {
    value: Mutable<i32>,
}

impl mika::Component for CounterComp {
    type State = Counter;
    type ListChange = ();
    type Output = mika::dom::Div;

    fn diff(&self, state: &Counter, _: ()) {
        self.value.set_neq(state.value);
    }

    fn render(&self, comp: &mika::CompHandle<Self>) -> Self::Output {
        mika::dom::Div::new()
            .child(
                crate::stateless_view_renders::CounterView {
                    title: &format!("Counter with step={}", comp.state().step),
                    signal: self.value.signal(),
                    up: mika::handler!(comp, Counter::up),
                    down: mika::handler!(comp, Counter::down),
                }
                .render(),
            )
            .into()
    }
}
