use wasm_bindgen::JsCast;

#[derive(Clone, PartialEq)]
pub struct TodoItem {
    pub id: u32,
    pub title: String,
    pub completed: bool,
}

impl TodoItem {
    fn toggle(&mut self) {
        self.completed = !self.completed;
    }
    fn set_title(&mut self, title: String) {
        self.title = title;
    }
}

impl mika::Key for TodoItem {
    type Key = u32;
    fn key(&self) -> &Self::Key {
        &self.id
    }
}

pub struct TodoList {
    next_id: u32,
    pub todo_items: Vec<TodoItem>,
    pub editing_id: Option<u32>,
}

impl Default for TodoList {
    fn default() -> Self {
        TodoList {
            next_id: 1,
            todo_items: Vec::new(),
            editing_id: None,
        }
    }
}

impl TodoList {
    pub fn completed_count(&self) -> usize {
        self.todo_items.iter().fold(
            0,
            |count, item| if item.completed { count + 1 } else { count },
        )
    }

    pub fn toggle(&mut self, id: u32) {
        if let Some(item) = self.todo_items.iter_mut().find(|item| id == item.id) {
            item.toggle();
        }
    }

    pub fn toggle_all(&mut self) {
        let checked =
            self.todo_items.len() > 0 && (self.todo_items.len() != self.completed_count());
        self.todo_items
            .iter_mut()
            .for_each(|item| item.completed = checked)
    }

    pub fn clear_completed(&mut self) {
        self.todo_items.retain(|item| !item.completed)
    }

    pub fn remove_item(&mut self, id: u32) {
        self.todo_items.retain(|item| id != item.id)
    }

    pub fn add_new_todo(&mut self, event: web_sys::Event) {
        if let Some(target) = event.target() {
            let input: &web_sys::HtmlInputElement = target.unchecked_ref();
            let title = input.value();
            let title = title.trim();
            if title.is_empty() {
                return;
            }
            input.set_value("");
            let lc = self.todo_items.push(TodoItem {
                id: self.next_id,
                title: title.to_string(),
                completed: false,
            });
            self.next_id += 1;
            lc
        }
    }

    pub fn start_editing(&mut self, id: u32) {
        self.editing_id = Some(id)
    }

    pub fn end_editing(&mut self, event: web_sys::KeyboardEvent) {
        match event.key().as_str() {
            "Escape" => self.editing_id = None,
            "Enter" => {
                if let Some(target) = (event.as_ref() as &web_sys::Event).target() {
                    self.process_end_editing(target);
                }
            }
            _ => {}
        }
    }

    pub fn process_end_editing(&mut self, target: web_sys::EventTarget) {
        let input: &web_sys::HtmlInputElement = target.unchecked_ref();
        let title = input.value();
        let title = title.trim();
        if let Some(editing_id) = self.editing_id {
            self.editing_id = None;
            if title.is_empty() {
                self.todo_items.retain(|item| item.id != editing_id)
            } else if let Some(item) = self
                .todo_items
                .iter_mut()
                .find(|item| item.id == editing_id)
            {
                item.set_title(title.to_string());
            }
        }
    }

    pub fn blur_editing(&mut self, event: web_sys::FocusEvent) {
        if let Some(target) = (event.as_ref() as &web_sys::Event).target() {
            self.process_end_editing(target)
        }
    }
}
