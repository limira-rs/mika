use mika::dom::{
    Button, Div, Footer, Header, Input, InputType, Label, Li, Section, Span, Strong, Ul, A, H1, P,
};
use mika::prelude::*;
use signals::signal::{Mutable, SignalExt};
use signals::signal_vec::MutableVec;

mod state;

#[derive(Default)]
pub struct TodoComp {
    item_count: Mutable<usize>,
    item_left: Mutable<usize>,
    completed_count: Mutable<usize>,
    all_completed: Mutable<bool>,
    editing_id: Mutable<Option<u32>>,
    todo_items: MutableVec<state::TodoItem>,
}

impl mika::Component for TodoComp {
    type State = state::TodoList;
    type ListChange = mika::ListChange;
    type Output = mika::dom::Div;

    fn pre_render(_: &mika::CompHandle<Self>) -> Option<Vec<Box<dyn mika::events::Listener>>> {
        wasm_logger::init(wasm_logger::Config::new(log::Level::Debug).message_on_new_line());
        None
    }

    fn diff(&self, data: &state::TodoList, lc: mika::ListChange) {
        let item_count = data.todo_items.len();
        let completed_count = data.completed_count();

        self.item_count.set_neq(item_count);
        self.item_left.set_neq(item_count - completed_count);
        self.completed_count.set_neq(completed_count);
        self.all_completed
            .set_neq(item_count > 0 && item_count == completed_count);
        self.editing_id.set_neq(data.editing_id);
        lc.relay(&data.todo_items, &mika::Key::key, &self.todo_items);
    }

    fn render(&self, comp: &mika::CompHandle<Self>) -> Self::Output {
        mika::dom::Div::new()
            .child(
                Section::new()
                    .class("todoapp")
                    .child(
                        Header::new()
                            .class("header")
                            .child(H1::new().text("Mika Todos"))
                            .child(
                                Input::new()
                                    .class("new-todo")
                                    // It is recommended to use label instead of placeholder,
                                    // so, input.placeholder is not implemented. We still set
                                    // placeholder for consistency with TodoMVC specs.
                                    //.string_attribute("placeholder", "What needs to be done?")
                                    .placeholder("What needs to be done?")
                                    //.auto_focus(true)
                                    .on_change(mika::handler! {comp, state::TodoList::add_new_todo(:arg)}),
                            ),
                    )
                    .child(
                        Section::new()
                            .class("main")
                            .class_if_signal(
                                "hidden",
                                self.item_count.signal().map(|item_count| item_count == 0),
                            )
                            .child(
                                Input::new()
                                    .id("toggle-all")
                                    .class("toggle-all")
                                    .r#type(InputType::CheckBox)
                                    .checked_signal(self.all_completed.signal())
                                    .on_change(mika::handler! {comp, state::TodoList::toggle_all }),
                            )
                            .child(
                                Label::new()
                                    .r#for("toggle-all")
                                    .text("Mark all as complete"),
                            )
                            // .child(Render::todo_list(&self, comp)),
                            .child(
                                Ul::new().class("todo-list").keyed_list_signal_vec(
                                    comp,
                                    self.todo_items.signal_vec_cloned(),
                                ),
                            ),
                    )
                    .child(
                        Footer::new()
                            .class("footer")
                            .class_if_signal(
                                "hidden",
                                self.item_count.signal().map(|item_count| item_count == 0),
                            )
                            .child(Span::new().class("todo-count").child(
                                Strong::new().text_signal(self.item_left.signal().map(
                                    move |item_left| {
                                        format!(
                                            "{} {} left",
                                            item_left,
                                            if item_left == 1 { "item" } else { "items" }
                                        )
                                    },
                                )),
                            ))
                            .child(
                                Ul::new()
                                    .class("filters")
                                    // TODO: Implement routing
                                    .child(Li::new().child(A::new().href("#/").text("All")))
                                    .child(
                                        Li::new().child(A::new().href("#/active").text("Active")),
                                    )
                                    .child(
                                        Li::new()
                                            .child(A::new().href("#/completed").text("Completed")),
                                    ),
                            )
                            .child(
                                Button::new()
                                    .class("clear-completed")
                                    .class_if_signal(
                                        "hidden",
                                        self.completed_count
                                            .signal()
                                            .map(|completed_count| completed_count == 0),
                                    )
                                    .on_click(mika::handler! {comp, state::TodoList::clear_completed})
                                    .text("Clear completed"),
                            ),
                    ),
            )
            .child(
                Footer::new()
                    .class("info")
                    .child(P::new().text("Double-click to edit a todo"))
                    .child(P::new().text("Created by Limira as an example for the Mika framework"))
                    .child(
                        P::new()
                            .text("Part of ")
                            .child(A::new().href("http://todomvc.com").text("TodoMVC")),
                    ),
            )
            .into()
    }
}

#[mika::convert_to_impl_keyed_item]
impl state::TodoItem {
    fn render(&self, comp: &mika::CompHandle<TodoComp>) -> Li {
        let item_id = self.id;
        Li::new()
            .class_if("completed", self.completed)
            .class_if_signal(
                "editing",
                comp.render()
                    .editing_id
                    .signal()
                    .map(move |option| option == Some(item_id)),
            )
            .child(
                Div::new()
                    .class("view")
                    .child(
                        Input::new()
                            .class("toggle")
                            .r#type(InputType::CheckBox)
                            .checked(self.completed)
                            .on_change(mika::handler! {comp, state::TodoList::toggle(item_id)}),
                    )
                    .child(
                        Label::new()
                            .on_double_click(
                                mika::handler! {comp, state::TodoList::start_editing(item_id)},
                            )
                            .text(&self.title),
                    )
                    .child(
                        Button::new()
                            .class("destroy")
                            .on_click(mika::handler! {comp, state::TodoList::remove_item(item_id)}),
                    ),
            )
            .child(
                Input::new()
                    .class("edit")
                    .value(&self.title)
                    .focus_signal(
                        comp.render()
                            .editing_id
                            .signal()
                            .map(move |option| option == Some(item_id)),
                    )
                    .on_blur(mika::handler! {comp, state::TodoList::blur_editing(:arg)})
                    .on_key_press(mika::handler! {comp, state::TodoList::end_editing(:arg)}),
            )
            .into()
    }
}
