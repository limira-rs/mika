use signals::signal::Mutable;
use signals::signal_vec::MutableVec;
use wasm_bindgen::UnwrapThrowExt;

pub struct TodoItem {
    pub id: u32,
    pub title: Mutable<String>,
    pub completed: Mutable<bool>,
}

pub struct TodoList(MutableVec<std::rc::Rc<TodoItem>>);

impl TodoList {
    pub fn new() -> Self {
        TodoList(MutableVec::new())
    }

    pub(crate) fn counter(&self) -> super::Counter {
        let completed_item_count = self.0.lock_ref().iter().fold(0, |count, item| {
            if item.completed.get() {
                count + 1
            } else {
                count
            }
        });
        super::Counter {
            all_item_count: self.0.lock_ref().len(),
            completed_item_count,
        }
    }

    pub fn add(&self, id: u32, title: &str) {
        self.0.lock_mut().push_cloned(std::rc::Rc::new(TodoItem {
            id,
            title: Mutable::new(title.to_string()),
            completed: Mutable::new(false),
        }));
    }

    pub fn find_index(&self, id: u32) -> usize {
        self.0
            .lock_ref()
            .iter()
            .position(|item| item.id == id)
            .expect_throw("No item has the given id")
    }

    pub fn remove_by_id(&self, id: u32) {
        let index = self.find_index(id);
        //log::info!("Removing item id {} at index {}", id, index);
        self.0.lock_mut().remove(index);
    }

    pub fn remove_by_index(&self, index: usize) {
        self.0.lock_mut().remove(index);
    }

    pub fn toggle(&self, id: u32) {
        self.0.lock_ref()[self.find_index(id)]
            .completed
            .replace_with(|v| !*v);
    }

    pub fn toggle_all(&self, checked: bool) {
        self.0
            .lock_ref()
            .iter()
            .for_each(|item| item.completed.set_neq(checked));
    }

    pub fn clear_completed(&self) {
        self.0.lock_mut().retain(|i| !i.completed.get());
    }

    pub fn list(&self) -> &MutableVec<std::rc::Rc<TodoItem>> {
        &self.0
    }

    pub fn update_title(&self, index: usize, title: String) {
        self.0.lock_ref()[index].title.set_neq(title);
    }
}
