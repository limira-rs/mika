use mika::prelude::*;
use serde::Deserialize;
use signals::signal::SignalExt;
use wasm_bindgen::prelude::*;

#[derive(Clone, Deserialize)]
struct Commit {
    id: String,
    short_id: String,
    title: String,
    created_at: String,
    message: String,
    author_name: String,
    author_email: String,
    authored_date: String,
}

#[derive(Clone, Deserialize)]
struct Branch {
    name: String,
    commit: Commit,
}

struct AppState {
    message: signals::signal::Mutable<Option<String>>,
    master_branch_info: signals::signal::Mutable<Option<Branch>>,
}

mika::create_raw_app_handle!(AppState);

impl Default for AppState {
    fn default() -> Self {
        Self {
            message: signals::signal::Mutable::new(Some("Please click the button".to_string())),
            master_branch_info: signals::signal::Mutable::new(None),
        }
    }
}

impl AppState {
    fn set_data(&self, data: Branch) {
        self.master_branch_info.set(Some(data));
        self.message.set(None);
    }
    fn set_error(&self, error: mika::Error) {
        self.message.set(Some(error.to_string()));
    }
    fn start_fetch(&self, app: &std::rc::Rc<Self>) {
        let url = "https://gitlab.com/api/v4/projects/12457005/repository/branches/master";
        mika::fetch::Request::new(mika::fetch::Method::Get, url).fetch_json_ok_error(
            mika::raw_handler!(app.set_data(_)),
            mika::raw_handler!(app.set_error(_)),
        );
        self.message.set(Some(
            "A fetch request is sent. Please wait for (a few) seconds".to_string(),
        ));
    }
}

fn tr(title: &str, value: &str) -> mika::dom::Tr {
    mika::dom::Tr::new()
        .child(mika::dom::Th::new().text(title))
        .child(mika::dom::Td::new().text(value))
        .into()
}

impl mika::RawComponent for AppState {
    type RenderOutput = mika::dom::Div;
    fn render(app: &std::rc::Rc<Self>) -> Self::RenderOutput {
        mika::dom::Div::new()
            .child(mika::dom::H1::new().text("Mika latest commit info"))
            .child_signal(app.message.signal_cloned().map({
                let app = std::rc::Rc::clone(app);
                move |value| match value {
                    None => mika::dom::Div::new()
                        .child(mika::dom::P::new().text("Fetched successfully")),
                    Some(text) => mika::dom::Div::new()
                        .child(mika::dom::P::new().text(&text))
                        .child(
                            mika::dom::Button::new()
                                .text("Fetch Mika latest commit info")
                                .on_click(mika::raw_handler!(app.start_fetch(&app))),
                        ),
                }
            }))
            .child_signal(app.master_branch_info.signal_cloned().map(move |value| {
                match value {
                    None => mika::dom::Table::new(),
                    Some(data) => mika::dom::Table::new()
                        .child(tr("Branch", &data.name))
                        .child(tr("Commit id", &data.commit.id))
                        .child(tr("Title", &data.commit.title))
                        .child(tr("Created at", &data.commit.created_at))
                        .child(tr("Message", &data.commit.message))
                        .child(tr("Author name", &data.commit.author_name))
                        .child(tr("Authored date", &data.commit.authored_date))
                        .into(),
                }
            }))
            .into()
    }
}
