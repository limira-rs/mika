use wasm_bindgen::prelude::*;

// All code are moved into the `share-lib` for sharing with the demos in the /guide.

// The component Counters is define in /examples/share-lib/src/counters.rs
// We just import it here and create an app handle for it.
mika::create_raw_app_handle! {raw_share_lib::counters::Counters}
