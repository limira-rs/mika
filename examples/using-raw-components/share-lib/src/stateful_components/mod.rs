use mika::prelude::*;
use signals::signal::{Mutable, SignalExt};
use wasm_bindgen::{JsCast, UnwrapThrowExt};

mod components;

// Or a render function
fn mode_list<F: mika::events::Change>(on_change: F) -> mika::dom::FieldSet {
    mika::dom::FieldSet::new()
        .child(mika::dom::Legend::new().text("Select a mode"))
        .child(
            mika::dom::Select::new()
                .multiple(true)
                .size(2)
                .on_change(on_change)
                .child(
                    mika::dom::Option::new()
                        .text("Counter 1")
                        .value("counter_1"),
                )
                .child(
                    mika::dom::Option::new()
                        .text("Counter 2")
                        .value("counter_2"),
                ),
        )
        .into()
}

#[derive(Clone)]
enum Mode {
    Counter1,
    Counter2,
}

pub struct MainComp {
    mode: Mutable<Mode>,
    value: Mutable<i32>,
    counter1: std::cell::RefCell<Option<std::rc::Rc<components::Counter1<Self>>>>,
}

impl Default for MainComp {
    fn default() -> Self {
        Self {
            mode: Mutable::new(Mode::Counter1),
            value: Mutable::new(0),
            counter1: std::cell::RefCell::new(None),
        }
    }
}

impl components::CommunicateWithCounter1 for MainComp {
    fn set_counter_1(&self, c1: &std::rc::Rc<components::Counter1<Self>>) {
        self.counter1.borrow_mut().replace(std::rc::Rc::clone(c1));
    }
    fn set_value_from_counter_1(&self, value: i32) {
        self.value.replace(value);
    }
}

impl MainComp {
    fn remove_counter_1(&self) {
        self.counter1.borrow_mut().take();
    }
    fn change_mode(&self, mode: Mode) {
        self.mode.replace(mode);
    }
    fn select_mode(&self, event: web_sys::Event) {
        let target = event.target().expect_throw("select_mode: event.target()");
        let select: &web_sys::HtmlSelectElement = target.unchecked_ref();
        match select.value().as_str() {
            "counter_1" => self.change_mode(Mode::Counter1),
            "counter_2" => self.change_mode(Mode::Counter2),
            _ => {}
        }
    }
}

impl mika::RawComponent for MainComp {
    type RenderOutput = mika::dom::Div;
    fn init(_: &std::rc::Rc<Self>) {
        wasm_logger::init(wasm_logger::Config::new(log::Level::Debug).message_on_new_line());
    }
    fn render(comp: &std::rc::Rc<Self>) -> Self::RenderOutput {
        mika::dom::Div::new()
            .child(mika::dom::H2::new().text("Counters"))
            .child(mika::dom::P::new().text("In stateful components"))
            .child(mika::dom::Hr::new())
            .child(mode_list(mika::raw_handler! {comp.select_mode(_)}))
            .child(
                mika::dom::P::new().text_signal(
                    comp.value
                        .signal()
                        .map(|value| format!("Value of the main component: {}", value)),
                ),
            )
            .child(
                mika::dom::FieldSet::new()
                    .child(mika::dom::Legend::new().text("Active component"))
                    .raw_component_signal(comp.mode.signal_cloned().map({
                        let main_comp = std::rc::Rc::clone(&comp);
                        move |mode| match mode {
                            Mode::Counter1 => components::Counter1::new(&main_comp).into(),
                            Mode::Counter2 => {
                                main_comp.remove_counter_1();
                                components::Counter2::new().into()
                            }
                        }
                    })),
            )
            .child(
                mika::dom::FieldSet::new()
                    .child(mika::dom::Legend::new().text("This is a permanent component"))
                    .child(
                        mika::dom::Div::new().raw_component(components::PermanentComponent::new()),
                    ),
            )
            .into()
    }
}
