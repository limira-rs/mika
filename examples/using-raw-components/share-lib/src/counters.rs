use mika::prelude::*;
use signals::signal::Mutable;

pub struct Counters {
    year: Mutable<i32>,
    month: Mutable<i32>,
}

impl Default for Counters {
    fn default() -> Self {
        Self {
            year: Mutable::new(2019),
            month: Mutable::new(6),
        }
    }
}

impl Counters {
    fn year_up(&self) {
        self.year.replace_with(|y| *y + 1);
    }
    fn year_down(&self) {
        self.year.replace_with(|y| *y - 1);
    }
    fn month_up(&self) {
        self.month.replace_with(|m| *m + 1);
        if self.month.get() > 12 {
            self.month.replace(1);
            self.year_up();
        }
    }
    fn month_down(&self) {
        self.month.replace_with(|m| *m - 1);
        if self.month.get() < 1 {
            self.month.replace(12);
            self.year_down();
        }
    }
}

impl mika::RawComponent for Counters {
    type RenderOutput = mika::dom::Div;
    fn render(state: &std::rc::Rc<Self>) -> Self::RenderOutput {
        mika::dom::Div::new()
            .child(mika::dom::H2::new().text("Counters"))
            .child(mika::dom::P::new().text("Two counters reuse a stateless component"))
            .child(mika::dom::Hr::new())
            .child(
                crate::counter_view::CounterView {
                    title: "Year",
                    signal: state.year.signal(),
                    up: mika::raw_handler!(state.year_up()),
                    down: mika::raw_handler!(state.year_down()),
                }
                .render(),
            )
            .child(
                crate::counter_view::CounterView {
                    title: "Month",
                    signal: state.month.signal(),
                    up: mika::raw_handler!(state.month_up()),
                    down: mika::raw_handler!(state.month_down()),
                }
                .render(),
            )
            .into()
    }
}
