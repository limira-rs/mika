With Component, you still have to restructure your logic a bit when working with Vec. Your Vec<T> still be in your app's state without change. But

It's a little cumbersome when working with Component:
* Have to relay the changes
* Have to tell Mika what change you made with a Vec and only one change is allow in each update. You can do multiple updates on your Vec and tell Mika to rerender_all items (of course, it is inefficient).


RawComponent, Component, CustomElement all require its root element is a FlowContent