# Signals or not signals

*! You can skip this section and go to First example !*

Personally I don't like frameworks with virtual DOM approach. I don't like the fact that on every state update, the whole new virtual DOM is rendered and then diffing with the current virtual DOM. [`signals`](https://github.com/Pauan/rust-signals) help trigger the right part of the render code to update the relevant portion of the view. But after one (or two) month(s) using it, I don't like it because of the verbosity and severely reducing code reuse of backend code on frontend.

## Verbosity

You have to (when accessing your data) `.lock_ref()`, `.lock_mut()`, `.get()`, `.get_cloned()`, `.replace_with()`... everywhere.

## Code reuse

If you build a frontend application in Rust, it's very high possibility that your backend is also in Rust. Because they are both in Rust, you want to reuse code that is the same on both ends. If you use a virtual DOM frameworks, nothing prevent you from sharing code. But if you use a signals-based frameworks like Dominator or Mika's `RawComponent`, problems arise. Let's build a TodoList application that stores data on the backend to see the problem.

On the backend:
```rust
struct TodoItem {
    id: u32,
    title: String,
    completed: bool,
}

impl TodoItem {
    fn is_completed(&self) -> bool {
        self.completed
    }
}

struct TodoList {
    items: Vec<TodoItem>,
}

impl TodoList {
    fn count_completed_items(&self) -> usize {
        self.items.iter().filter(|item| item.completed).count()
    }
}
```
Using signals, on the frontend:
```rust
#[derive(Clone)]
struct TodoItem {
    id: u32,
    title: Mutable<String>,
    completed: Mutable<bool>,
}

impl TodoItem {
    fn is_completed(&self) -> bool {
        self.completed.get()
    }
}

struct TodoList {
    items: MutableVec<TodoItem>,
}

impl TodoList {
    fn count_completed_items(&self) -> usize {
        self.items.lock_ref().iter()
            .filter(|item| item.completed.get()).count()
    }
}
```
The methods named `is_completed` and `count_completed_items` need different implementations on each sides, therefore unable to be shared. Well, the example is tiny, it is easy to find a way to mitigate the problem. But for larger data, it is not easy any more.

# Move `signals` into a separate render

Still don't like to go back to vDOM approach, I try to isolate `signals` into the render only. Mika's `Component` allow you using normal Rust's `struct`/`enum`s in the state. Its render is a separated `struct` which contains data wraps in `Mutable`/`MutableVec`.

Working with `Component`, you have duplicated data in the state and in the render, and the performance suffer a little. But it is more easy to work with the component's state and you can reuse more code from the backend on your frontend. Well, you still have to deal with `signals` in the render.