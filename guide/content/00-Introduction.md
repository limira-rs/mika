# Introduction

Mika is a framework for building frontend application in Rust. Mika is inspired by [Dominator](https://github.com/Pauan/rust-dominator) and (originally) by [Yew](https://github.com/yewstack/yew).

* Mika is still in **experimental** stage.
* Breaking changes occur frequently.
* No release yet
* No #[test] yet.
* Mika is a bit verbose and requires you to do weird things.

**Minimum Rust version: 1.39**

## Compare with Dominator and Yew

|                                | Mika                                   | Dominator                         | Yew                              |
| ------------------------------ | -------------------------------------- | --------------------------------- | -------------------------------- |
| Status                         | experimental                           | more mature                       | more mature                      |
| Requires NodeJS                | ✗                                      | ✗                                 | ✗                                |
| Type?                          | signal based, hybrid?                  | signal based                      | vDOM                             |
| Separation of state vs view    | ✓ encourage                            | ✗ ?                               | ✓ ?                              |
| Build the DOM                  | mainly Rust code + some macros         | `dominator::html!` + other macros | `yew::html!`                     |
| Component                      | ✓                                      | ✗(1) - has alternative features   | ✓                                |
| * Elm-like messages            | ✗                                      | ✗                                 | ✓                                |
| * Diffing                      | on data                                | ✗                                 | new vDOM vs old vDOM             |
| * Update view on state change  | diffing on data, execute relevant code | only execute relevant code        | render, diffing vDOM + patch DOM |
| * Duplication                  | duplicate of data (permanently)        | ✗                                 | duplicate of vDOM (temporarily)  |
| * Update by parent             | ✗ (need explicit call)                 | -                                 | ✓                                |
| * Parent pass `props` to child | ✗                                      | -                                 | ✓                                |
| * Share code with Rust backend | most                                   | some                              | most                             |
| Validate child elements        | ✓ try to validate at compile-time(2)   | ?                                 | ?                                |
| Validate element's attributes  | ✓ try to validate at compile-time(3)   | ?                                 | ?                                |

(*) Items preceded by * only apply to Mika/Yew's `Component`. Dominator does not support such kind of component, but it has alternative features which are very powerful and flexible.

* (1) You can implement component by yourself outside of Dominator
* (2) Mika tries to validate child element at compile-time (by trait bound). For example, you will never accidentally add an `<option>` to a `<div>`. But it only limits to direct children. Mika is unable to check for the validation of children of a child of an element.
* (3) Mika tries to validate an element's attributes by implement methods for every common attributes.

Mika embraces `Component` now. But the original idea was similar Dominator. The original implement is now still available in Mika with the name: `RawComponent`, it just offers some features of Dominator.

## What a Mika app look like?

I **think** (may change :D) an application typically consists of one component that is either a `Component` or a `RawComponent`. Render's code can be split (and reuse) by using Rust functions or builder `struct`s with a render method that returns an html element. On response to a change that occurs in the component state, only view-code that were assigned/relevant to that change will be executed. So you don't need to break your app into smaller components just because of performance (but if you do, it is because of other reasons).

Multiple `RawComponent`/`Component`s are allowed in an application, but you must explicitly send data between them (**not** passing `props` down like in React or Yew).

I think a `RawComponent`/`Component` should only use for an isolated part or big things that need their own state and message such as a rich text editor, a [jigsaw canvas](https://www.jigsawplanet.com/?rc=play&pid=30055abcabe1), a map - like Google Maps (is it possible to build them with Mika?).

## Plan

These are plans to improve user experience. (Other features will definitely be added in future, such as `routing`, `svg`...)

* do experiment to introduce procedural macros (derive or custom attribute):
  * only apply to `Component::Render` (not support for `RawComponent`)
  * to hide `signals` from users (auto generate code from component's state and html (or similar) template)
  * to exploit template for list item (for better performance, auto generate code from a template for `mika::app::ListItem`'s `render` and `update`)
  * if this is successful, working with `Component` is (to some extent) similar to a virtual DOM framework but better performance
* do experiment to implement some kind of simple `notify` or `observable`/`observer` (which does not rely on Future) to replace `signals`
  * (again in `Component::Render` only).
  * The main goals are to reduce binary size and compilation time (but the performance must be similar to `signals`).
  * If the experiment is successful then all `signals`-related things (include `RawComponent` which will always bases on `signals`) will be gated behind a Cargo feature. 

But for now, they are just plans. I am now will focus on my project, and improve Mika for my own use case. So, I probably will not have enough time to do plans above in the foreseeable future. Any help are welcome!

TODO:
* *Try* to make users aware of **known vulnerabilities** (e.g. cause by using some particular attributes).
* `CustomElement` (not related to [this](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements)) for wrapping around an element that render by a JavaScript library.
