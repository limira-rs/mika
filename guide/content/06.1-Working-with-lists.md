# Working with lists

You may already noticed that the `Component` trait requires to specify an associated type named `ListChange` in previous examples. The idea arose when I was using Yew. If I understand correctly, Yew re-renders the whole virtual DOM on every state update. If you highlight/remove an item from your list, Yew will re-render the whole list to its vDOM, then find the diff to apply to the physical DOM. I wondered why do we not tell the framework what has been changed? For example, the item at `index=4` has beed removed?

The idea is successfully implemented in Mika, but it is very unpleasant to use. So I implement a *fallback* mode which finds diffs on list. Yew performs diffing on vDOM, which only available after rendering. Mika performs diffing on the data (a `Vec` in your component's state and its clone in the render's state).

`ListChange` is still very useful when you want to tune the amount of diffing that some updates have to perform. For example, when a change does not involve to your list, you may want to tell Mika to ignore the list in the subsequence update. Here is how to add `ListChange` to the definition of component:

```rust
// This is required:
use mika::prelude::*;

pub struct TodoList {
    pub todo_items: Vec<TodoItem>,
    // Some items omitted
}

impl TodoList {
    fn default_behavior_for_list(&mut self) {
        // We ignore the context in this method, then Mika always do diffing on
        // our list.
    }

    // This is a method that update the state. And we want to tell Mika
    // what we have done with our list.
    // First, the method must return a Context:
    fn new_item(&mut self) -> mika::Context<TodoListComp> {
        let mut context = mika::Context::default();
        // Then, tell Mika the exact change you have done via `context.list_change`
        context.list_change = self.todo_items.mika_push(TodoItem::new());
        // The default value of `context.list_change` is ListChange::diffing().
        // If we ignore the `context.list_change`, Mika will do diffing.
        
        context
    }

    // If you want to ignore the diffing on the list when update the state by this method:
    fn change_that_not_involve_todo_items(&mut self) -> mika::Context<TodoListComp> {
        let mut context = mika::Context::default();
        // We tell Mika to do nothing for the list when relay changes from state to render
        context.list_change.set_no_change();

        context
    }
}

#[derive(Default)]
pub struct TodoListComp {
    // A clone of the state's Vec of TodoItem, but wrapped in `MutableVec`
    todo_items: MutableVec<super::TodoItem>,
    // Some items omitted
}


impl mika::Component for TodoListComp {
    type State = TodoList;
    // Tell Mika that we want to notify what change has been done to a list.
    type ListChange = mika::ListChange;
    type Output = mika::dom::Div;

    fn diff(&self, state: &Self::State, list_change: Self::ListChange) {
        //                              ^^^^^^^^^^^
        //                              the value of ListChange is passed
        //                              here from the Context. If you do
        //                              not explicitly set it via a Context,
        //                              the default value is `ListChange::diffing()`

        // Relay the changes from state to the MutableVec.
        // The value of `list_change` affects how the relay occurs.
        list_change.relay(&state.todo_items, &mika::Key::key, &self.todo_items);
        //                                   ^^^^^^^^^^^^^^^
        //                                   Use this (`&mika::Key::key`) if item type
        //                                   in state and in component are the same.
        //                                   For more information, see the section
        //                                   *Lists/Keep framework away your core code*

        // Some items omitted
    }

    fn render(&self, comp: &mika::CompHandle<Self>) -> Self::Output {
        // No change is required in the render method
    }
}
```

`examples/share-lib/todomvc/*` contains a working example of list. But it does not try to modify the default behavior of the list change.