# Stateful components

The demo in this section consists of 3 components: a main component and two sub components. The main component has its own counter value and a box allow you to select which sub component should be in action.

In mode `Normal counter`, the sub component just start its counter at the value taken from the main component, no other communication with the main component.

In mode `Send back counter`, a change in the value of the main component also update the value in the sub component. The sub component count independently, but can send its new value to the main component on request.

# Should I have multi components in a Mika app?

Despite Mika allow adding sub components to its app, I **think** in most cases a single component is enough. As long as you are happy to manage your app state (data) in a single component, just use one component for your app. The reasons:

* You are able to split your render code (of one component) into smaller pieces (see *Stateless render*)
* The size of the state should (I *think*) just has a little affect on the performance of the app in case of `Component`, and no affect in case of `RawComponent`.

That said, there are still cases sub components are useful, for examples:

* An isolated part of your big app that has its own event (messages) and standalone data.
* A [jigsaw canvas](https://www.jigsawplanet.com/?rc=play&pid=30055abcabe1) component. I think this deserve a separate component despite how small the main app is.
* A map component - like Google Maps.
* A rich text editor component.

(is it possible to build them with Mika?)

See example code in [`examples/share-lib/src/stateful_components/*`](https://gitlab.com/limira-rs/mika/tree/master/examples/share-lib/src/stateful_components).