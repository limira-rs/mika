#!/bin/bash

set -ex

# Workaround clippy refuse to work on compiled crate
touch ./src/lib.rs
cargo clippy -- -D warnings
cargo clippy --manifest-path=macros/Cargo.toml -- -D warnings

cargo test

./build-examples.sh

wasm-pack build --out-dir ../mika --target web --no-typescript ./guide
