// This execute on the whole ./elements.rs content
macro_rules! create_builders_for_html_elements {
    ($(
        $(#[$meta_content:meta])+
        $html_tag:ident
        $ElementType:ident
        $($ElementWithChildType:ident)?
        $(: $($Category:ident)+ )?
        [$($tt:tt)*]
    )+) => {
        $(
            create_html_element_builder! {
                $(#[$meta_content])+
                $html_tag
                $ElementType
                $($ElementWithChildType)?
                [$( $($Category)+ )?]
                [$($tt)*]
            }
        )+
    };
}

// Each line of ./elements.rs
macro_rules! create_html_element_builder {
    // Element that not allow children
    (
        $(#[$meta_content:meta])+
        $html_tag:ident
        $ElementType:ident
        [$($Category:ident)*]
        []
    ) => {
        $(#[$meta_content])+
        pub struct $ElementType(TotalElement);
        impl $ElementType {
            pub fn new() -> Self {
                Self(TotalElement::with_tag(stringify!($html_tag)))
            }

            pub fn with_id(id: &str) -> Result<Self, crate::errors::Error> {
                TotalElement::with_id_and_tag(id, stringify!($html_tag))
                    .map(Self)
            }

            pub fn total_element_mut(&mut self) -> &mut TotalElement {
                &mut self.0
            }
        }
        impl Default for $ElementType {
            fn default() -> Self {
                Self::new()
            }
        }
        implement_traits_for_html_element_builder! { $ElementType [$($Category)*] CloneWsElement }
    };

    // Element that allow children
    (
        $(#[$meta_content:meta])+
        $html_tag:ident
        $ElementType:ident
        $ElementWithChildType:ident
        [$($Category:ident)*]
        [
            $(#[$child_meta:meta])*
            $ChildTrait:ident
            $($tt:tt)*
        ]
    ) => {
        create_html_element_builder!{
            $(#[$meta_content])+
            $html_tag
            $ElementType
            [$($Category)*]
            []
        }

        implement_child_methods_for_html_element_builder! {
            $ElementType
            $(#[$child_meta])*
            $ChildTrait
            $ElementWithChildType
        }

        // Create the ChildedDiv... types and implement their methods
        $(#[$meta_content])+
        pub struct $ElementWithChildType(TotalElement);
        impl From<$ElementType> for $ElementWithChildType {
            fn from(item: $ElementType) -> Self {
                Self(item.0)
            }
        }
        impl From<$ElementWithChildType> for $ElementType {
            fn from(item: $ElementWithChildType) -> Self {
                Self(item.0)
            }
        }
        implement_traits_for_html_element_builder! { $ElementWithChildType [$($Category)*] }
        implement_child_methods_for_html_element_builder! {
            $ElementWithChildType
            $(#[$child_meta])*
            $ChildTrait
            $ElementWithChildType
        }

        //
        impl $ElementType {
            $(#[$child_meta])*
            pub fn child_list<T, I>(self, iter: I) -> ClosedElement<Self>
            where
                T: Into<TotalElement> + $ChildTrait,
                I: std::iter::Iterator<Item = T>,
            {
                self.0.child_list(iter);
                ClosedElement::new(self)
            }

            $(#[$child_meta])*
            pub fn child_list_signal<E, S>(mut self, signal: S) -> ClosedElement<Self>
            where
                E: 'static + Into<TotalElement> + $ChildTrait,
                S: 'static + signals::signal::Signal<Item = Vec<E>>,
            {
                self.0.child_list_signal(signal);
                ClosedElement::new(self)
            }

            $(#[$child_meta])*
            pub fn list_signal_vec<E, S>(mut self, signal: S) -> ClosedElement<Self>
            where
                E: 'static + Into<TotalElement> + $ChildTrait,
                S: 'static + signals::signal_vec::SignalVec<Item = E>,
            {
                self.0.list_signal_vec(signal);
                ClosedElement::new(self)
            }

            $(#[$child_meta])*
            pub fn keyed_list_signal_vec<C, S, T>(mut self, comp: &crate::app::CompHandle<C>, signal: S) -> ClosedElement<Self>
            where
                C: 'static + crate::app::Component,
                T: 'static + crate::list::KeyedItem<C>,
                <T as crate::list::Key>::Key: Clone,
                S: 'static + signals::signal_vec::SignalVec<Item = T>,
            {
                self.0.keyed_list_signal_vec(comp, signal);
                ClosedElement::new(self)
            }
        }

        impl From<ClosedElement<$ElementType>> for TotalElement {
            fn from(item: ClosedElement<$ElementType>) -> Self {
                (item.0).0
            }
        }
        impl From<ClosedElement<$ElementType>> for $ElementType {
            fn from(item: ClosedElement<$ElementType>) -> Self {
                item.0
            }
        }


        mark_as_possible_child! { $ChildTrait $($tt)* }
        implement_component_methods_for_html_element_builder! { ($($tt)*) $ElementType $ElementWithChildType }
        implement_component_methods_for_html_element_builder! { ($ChildTrait) $ElementType $ElementWithChildType }
        implement_traits_for_closed_element! { $ElementType $($Category)* }
    };
}

// Implement traits for A Div ChildedDiv...
macro_rules! implement_traits_for_html_element_builder {
    ($Builder:ident [$($Category:ident)*] $($CloneWsElement:ident)?) => {
        $(
            impl $Category for $Builder {}
        )*

        impl From<$Builder> for TotalElement {
            fn from(item: $Builder) -> Self {
                item.0
            }
        }

        impl HasWsNode for $Builder {
            fn ws_node(&self) -> &web_sys::Node {
                self.0.ws_node()
            }
        }

        impl HasWsElement for $Builder {
            fn ws_element(&self) -> &web_sys::Element {
                &self.0.ws_element
            }
        }

        impl GlobalAttributes for $Builder {}

        impl AsMut<TotalElement> for $Builder {
            fn as_mut(&mut self) -> &mut TotalElement {
                &mut self.0
            }
        }

        impl StoreHandle for $Builder {
            fn store_future_handle(
                &mut self,
                handle: discard::DiscardOnDrop<signals::CancelableFutureHandle>,
            ){
                self.0.store_future_handle(handle);
            }

            fn store_listener(&mut self, listener: Box<dyn crate::events::Listener>){
                self.0.store_listener(listener);
            }
        }

        $(
            impl $CloneWsElement for $Builder {
                fn clone_deep(&self) -> Self {
                    Self(
                        TotalElement::with_ws_element(
                            self.0.ws_element
                                .clone_node_with_deep(true)
                                .expect_throw("Deep clone the template")
                                .unchecked_into()
                        )
                    )
                }
            }
        )?
    };
}

// Each $Child must mark with $ChildTrait to be allowed as a child of an element that accept $ChildTrait
macro_rules! mark_as_possible_child {
    ( $ChildTrait:ident ) => {
        // No implementation needed
    };
    ($ChildTrait:ident $(: $MainCategory:ident +)? [$($Child:ident)+]) => {
        pub trait $ChildTrait {}
        $(
            impl $ChildTrait for $Child {}
        )+
        $(
            impl<T:$MainCategory> $ChildTrait for T {}
        )?
    };
}

macro_rules! implement_child_methods_for_html_element_builder {
    ($Builder:ident $(#[$child_meta:meta])* $ChildTrait:ident $ChildBuilder:ident) => {
        impl $Builder {
            $(#[$child_meta])*
            pub fn child<T: Into<TotalElement> + $ChildTrait>(self, child: T) -> $ChildBuilder {
                self.0.child(child);
                self.into()
            }

            $(#[$child_meta])*
            pub fn child_signal<S, T>(mut self, signal: S) -> $ChildBuilder
            where
                T: 'static + Into<TotalElement> + $ChildTrait,
                S: 'static + signals::signal::Signal<Item = T>,
            {
                self.0.child_signal(signal);
                self.into()
            }
        }
    };
}

macro_rules! implement_component_methods_for_html_element_builder {
    ( (: FlowContent $($tt:tt)*) $Builder:ident $ChildBuilder:ident ) => {
        implement_component_methods_for_html_element_builder! {
            (FlowContent) $Builder $ChildBuilder
        }
    };
    ( (FlowContent $($tt:tt)*) $Builder:ident $ChildBuilder:ident ) => {
        impl AllowComponent<$ChildBuilder> for $Builder {}
        impl AllowComponent<$ChildBuilder> for $ChildBuilder {}
    };
    ( ($($NotFlowContent:tt)*) $Builder:ident $ChildBuilder:ident ) => {
        // Not implement component method for element that not accept FlowContent
    };
}

macro_rules! implement_traits_for_closed_element {
    ($ElementType:ident AllowText $($Category:ident)*) => {
        implement_traits_for_closed_element! {
            $ElementType $($Category)*
        }
    };
    ($ElementType:ident $($Category:ident)*) => {
        $(
            impl $Category for ClosedElement<$ElementType> {}
        )*
    };
}

//==============================================================================
//==============================================================================
//==============================================================================
// Macros for generating attribute methods
//==============================================================================
//==============================================================================
//==============================================================================

// All lines in ./attributes.rs
macro_rules! implement_attribute_methods {
    ($(
        $is_signal:ident
        ($attr_name:ident)
        ($element_method:ident $total_element_method:ident)
        ($($type:tt)*)
        [$($Element:ident)*]
    )+) => {
        $(
        implement_attribute_method! {
            $is_signal
            ($attr_name)
            ($element_method $total_element_method)
            ($($type)*)
            [$($Element)*]
        }
        )+
    };
}

// Each line of ./attributes.rs
macro_rules! implement_attribute_method {
    (
        no_signal
        ($attribute_name:ident)
        ($element_method:ident $total_element_method:ident)
        ($($type:tt)+)
        [$($Element:ident)*]
    ) => {
        implement_attribute_method_for_total_element! {
            $attribute_name
            $total_element_method
            ($($type)+)
        }
        implement_attribute_method_for_elements! {
            $element_method
            $total_element_method
            ($($type)+)
            [$($Element)*]
        }
    };
    (
        signal
        ($attribute_name:ident)
        ($element_method:ident $total_element_method:ident)
        ($($type:tt)+)
        [$($Element:ident)*]
    ) => {
        implement_attribute_method_for_total_element! {
            $attribute_name
            $total_element_method
            ($($type)+)
        }
        implement_attribute_method_for_elements! {
            $element_method
            $total_element_method
            ($($type)+)
            [$($Element)*]
        }
        implement_attribute_method_signal_for_total_element! {
            $attribute_name
            $total_element_method
            ($($type)+)
        }
        implement_attribute_method_signal_for_elements! {
            $element_method
            $total_element_method
            ($($type)+)
            [$($Element)*]
        }
    };
}

macro_rules! implement_attribute_method_for_total_element {
    ( _ $($tt:tt)+ ) => {
        // No implementation here. The method has been specialized.
    };
    ($attribute_name:ident $method_name:ident (:$EnumType:ident)) => {
        impl TotalElement {
            #[allow(clippy::wrong_self_convention)]
            pub fn $method_name(&self, value: $EnumType) {
                self.attribute_string(stringify!($attribute_name), value.as_str());
            }
        }
    };
    ($attribute_name:ident $method_name:ident (&str)) => {
        implement_attribute_method_for_total_element! {
            $attribute_name
            $method_name
            attribute_string
            (&str)
        }
    };
    ($attribute_name:ident $method_name:ident ($type:ident)) => {
        paste::item! {
            implement_attribute_method_for_total_element! {
                $attribute_name
                $method_name
                [<attribute_ $type>]
                ($type)
            }
        }
    };
    ($attribute_name:ident $method_name:ident $typed_method_name:ident ($type:ty)) => {
        impl TotalElement {
            #[allow(clippy::wrong_self_convention)]
            pub fn $method_name(&self, value: $type) {
                self.$typed_method_name(stringify!($attribute_name), value);
            }
        }
    };
}

macro_rules! implement_attribute_method_for_elements {
    ($element_method:ident $total_element_method:ident (:$EnumType:ident) [$($Element:ident)*]) => {
        implement_attribute_method_for_elements! {
            $element_method
            $total_element_method
            ($EnumType)
            [$($Element)*]
        }
    };
    ($element_method:ident $total_element_method:ident ($type:ty) [$($Element:ident)*]) => {
        $(
            impl $Element {
                #[allow(clippy::wrong_self_convention)]
                pub fn $element_method(self, value: $type) -> Self {
                    (self.0).$total_element_method(value);
                    self
                }
            }
        )*
    };
}

macro_rules! implement_attribute_method_signal_for_total_element {
    (
        $attribute_name:ident
        $method_name:ident
        (:$EnumType:ident)
    ) => {
        paste::item! {
            impl TotalElement {
                #[allow(clippy::wrong_self_convention)]
                pub fn [<$method_name _signal>]<S>(&mut self, signal: S)
                where S: 'static + signals::signal::Signal<Item = $EnumType>
                {
                    let element = self.ws_element.clone();
                    self.spawn_for_each(signal, move |value| {
                        element.set_attribute(stringify!($attribute_name), value.as_str())
                            .expect_throw("string from enum signal generated by macro");
                    });
                }
            }
        }
    };
    (
        $attribute_name:ident
        $method_name:ident
        (&str)
    ) => {
        paste::item! {
            impl TotalElement {
                #[allow(clippy::wrong_self_convention)]
                pub fn [<$method_name _signal>]<S>(&mut self, signal: S)
                where S: 'static + signals::signal::Signal<Item = String>
                {
                    let element = self.ws_element.clone();
                    self.spawn_for_each(signal, move |value| {
                        element.set_attribute(stringify!($attribute_name), &value)
                            .expect_throw("string signal generated by macro");
                    });
                }
            }
        }
    };
    (
        $attribute_name:ident
        $method_name:ident
        (bool)
    ) => {
        paste::item! {
            impl TotalElement {
                #[allow(clippy::wrong_self_convention)]
                pub fn [<$method_name _signal>]<S>(&mut self, signal: S)
                where S: 'static + signals::signal::Signal<Item = bool>
                {
                    let element = self.ws_element.clone();
                    self.spawn_for_each(signal, move |value| {
                        if value {
                            element.set_attribute(stringify!($attribute_name), "")
                                .expect_throw("bool signal generated by macro");
                        }else{
                            element.remove_attribute(stringify!($attribute_name))
                                .expect_throw("bool signal generated by macro");
                        }
                    });
                }
            }
        }
    };
    (
        $attribute_name:ident
        $method_name:ident
        ($type:ident)
    ) => {
        paste::item! {
            impl TotalElement {
                #[allow(clippy::wrong_self_convention)]
                pub fn [<$method_name _signal>]<S>(&mut self, signal: S)
                where S: 'static + signals::signal::Signal<Item = $type>
                {
                    let element = self.ws_element.clone();
                    self.spawn_for_each(signal, move |value| {
                            element.set_attribute(stringify!($attribute_name), &value.to_string())
                                .expect_throw("$type signal generated by macro");
                    });
                }
            }
        }
    };
}

macro_rules! implement_attribute_method_signal_for_elements {
    ($element_method:ident $total_element_method:ident (:$EnumType:ident) [$($Element:ident)*]) => {
        implement_attribute_method_signal_for_elements! {
            $element_method
            $total_element_method
            ($EnumType)
            [$($Element)*]
        }
    };
    ($element_method:ident $total_element_method:ident (&str) [$($Element:ident)*]) => {
        implement_attribute_method_signal_for_elements! {
            $element_method
            $total_element_method
            (String)
            [$($Element)*]
        }
    };
    ($element_method:ident $total_element_method:ident ($type:ty) [$($Element:ident)*]) => {
        $(
            paste::item! {
                impl $Element {
                    #[allow(clippy::wrong_self_convention)]
                    pub fn [<$element_method _signal>]<S>(mut self, signal: S) -> Self
                    where S: 'static + signals::signal::Signal<Item = $type>
                    {
                        paste::expr! {
                            (self.0).[<$total_element_method _signal>](signal);
                        }
                        self
                    }
                }
            }
        )*
    };
}
