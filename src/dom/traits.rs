use wasm_bindgen::UnwrapThrowExt;

pub trait HasWsNode {
    fn ws_node(&self) -> &web_sys::Node;

    fn append_to(&self, parent: &web_sys::Node) {
        parent
            .append_child(self.ws_node())
            .expect_throw("HasWsNode::append_to:  parent.append_child(self.ws_node())");
    }

    fn insert_at(&self, index: usize, parent: &web_sys::Node) {
        let node_at_index = parent.child_nodes().get(index as u32);
        parent
            .insert_before(self.ws_node(), node_at_index.as_ref())
            .expect_throw(
                "HasWsNode::insert_at: parent.insert_before(self.ws_node(), node_at_index.as_ref())",
            );
    }

    fn replace(&self, parent: &web_sys::Node, old: &web_sys::Node) {
        parent
            .replace_child(self.ws_node(), old)
            .expect_throw("HasWsNode::replace: parent.replace_child(self.ws_node(), old)");
    }

    fn replace_at(&self, index: usize, parent: &web_sys::Node) {
        let node_at_index = parent
            .child_nodes()
            .get(index as u32)
            .expect_throw("HasWsNode::replace_at: parent.child_nodes().get");
        parent
            .replace_child(self.ws_node(), &node_at_index)
            .expect_throw(
                "HasWsNode::replace_at: parent.replace_child(self.ws_node(), &node_at_index)",
            );
    }

    fn remove_from(&self, parent: &web_sys::Node) {
        parent
            .remove_child(self.ws_node())
            .expect_throw("HasWsNode::remove_from: parent.remove_child(self.ws_node())");
    }
}

pub trait HasWsElement {
    fn ws_element(&self) -> &web_sys::Element;
}

pub trait CloneWsElement: HasWsNode {
    fn clone_deep(&self) -> Self;
}

pub trait StoreHandle {
    fn store_future_handle(
        &mut self,
        handle: discard::DiscardOnDrop<signals::CancelableFutureHandle>,
    );

    fn store_listener(&mut self, listener: Box<dyn crate::events::Listener>);
}

pub trait AllowText: Sized + AsMut<super::TotalElement> {
    fn text(mut self, text: &str) -> Self {
        self.as_mut().text(text);
        self
    }

    /// Same as `fn text` but process differently by macro that impl mika::KeyedItem.
    fn const_text(mut self, text: &str) -> Self {
        self.as_mut().text(text);
        self
    }

    // TODO: More efficient for things that is already a string such as: str, String...
    fn text_signal<S, T>(mut self, signal: S) -> Self
    where
        T: 'static + ToString,
        S: 'static + signals::signal::Signal<Item = T>,
    {
        self.as_mut().text_signal(signal);
        self
    }
}

pub trait EmbeddedContent: HasWsNode {}
pub trait FlowContent: HasWsNode {}
pub trait FormContent: HasWsNode {}
pub trait FormLabelableContent: HasWsNode {}
pub trait FormListedContent: HasWsNode {}
pub trait FormResettableContent: HasWsNode {}
pub trait FormSubmittableContent: HasWsNode {}
pub trait HeadingContent: HasWsNode {}
pub trait InteractiveContent: HasWsNode {}
pub trait MetadataContent: HasWsNode {}
pub trait PalpableContent: HasWsNode {}
pub trait PhrasingContent: HasWsNode {}
pub trait SectioningContent: HasWsNode {}
pub trait TransparentContent: HasWsNode {}

pub trait FlowNonInteractiveContent: HasWsNode {}
pub trait PhrasingNonInteractiveContent: HasWsNode {}
pub trait Headings: HasWsNode {}

macro_rules! implement_attribute_methods_in_trait {
    ($(
        $($name:ident)+: $type:ty,)
    +) => {
        $(
            implement_attribute_methods_in_trait! { @one_method $($name)+: $type }
        )+
    };
    (@one_method $name:ident: $type:ty) => {
        fn $name(mut self, value: $type) -> Self {
            self.as_mut().$name(value);
            self
        }
    };
    (@one_method $name:ident $total_element_method:ident: $type:ty) => {
        fn $name(mut self, value: $type) -> Self {
            self.as_mut().$total_element_method(value);
            self
        }
    };
}

macro_rules! implement_event_method_in_trait {
    ($($method_name:ident $EventType:ident,)+) => {
        $(
            fn $method_name<F: crate::events::$EventType>(mut self, f: F) -> Self {
                self.as_mut().$method_name(f);
                self
            }
        )+
    }
}

pub trait GlobalAttributes: Sized + AsMut<super::TotalElement> {
    fn custom_attribute(mut self, name: &str, value: &str) -> Self {
        self.as_mut().custom_attribute(name, value);
        self
    }

    fn template_custom_attribute(mut self, name: &str, value: &str) -> Self {
        self.as_mut().custom_attribute(name, value);
        self
    }

    fn class_if(mut self, class: &str, on: bool) -> Self {
        if on {
            self.as_mut().add_class(class);
        }
        self
    }

    fn class_if_signal<S>(mut self, class_name: &'static str, signal: S) -> Self
    where
        S: 'static + signals::signal::Signal<Item = bool>,
    {
        self.as_mut().class_if_signal(class_name, signal);
        self
    }

    fn focus_signal<S>(mut self, signal: S) -> Self
    where
        S: 'static + signals::signal::Signal<Item = bool>,
    {
        self.as_mut().focus_signal(signal);
        self
    }

    implement_attribute_methods_in_trait! {
        access_key: &str,
        auto_capitalize: super::AutoCapitalize,
        content_editable: bool,
        context_menu: &str,
        dir: super::Dir,
        draggable: bool,
        hidden: bool,
        id: &str,
        input_mode: super::InputMode,
        //is: &str,
        // anything: itemid, ..., itemtype: should be specialized?
        // TODO specialized type for Lang?
        lang: &str,
        spell_check: super::SpellCheck,
        style: &str,
        title: &str,
        class add_class: &str,
    }

    implement_event_method_in_trait! {
        on_blur Blur,
        on_focus Focus,
        on_click Click,
        on_double_click DoubleClick,
        on_change Change,
        on_key_press KeyPress,

    }
}
pub trait AllowComponent<T>: AsMut<super::TotalElement>
where
    Self: Into<T>,
{
    fn raw_component<C, E>(mut self, comp: C) -> T
    where
        C: 'static + crate::app::RawComponent<RenderOutput = E>,
        E: FlowContent + Into<super::TotalElement>,
    {
        self.as_mut().raw_component(comp);
        self.into()
    }

    fn raw_component_signal<S>(mut self, signal: S) -> T
    where
        S: 'static + signals::signal::Signal<Item = Box<dyn crate::app::FlowContentRawComponent>>,
    {
        self.as_mut().raw_component_signal(signal);
        self.into()
    }

    fn component<C, E>(mut self, comp: crate::app::Comp<C>) -> T
    where
        C: 'static + crate::app::Component<Output = E>,
        E: FlowContent + Into<super::TotalElement>,
    {
        self.as_mut().component(comp);
        self.into()
    }

    fn component_signal<S>(mut self, signal: S) -> T
    where
        S: 'static + signals::signal::Signal<Item = Box<dyn crate::app::FlowContentComponent>>,
    {
        self.as_mut().component_signal(signal);
        self.into()
    }

    fn custom_element<C, E>(mut self, comp: C) -> T
    where
        C: 'static + crate::app::CustomElement<Container = E>,
        E: 'static + FlowContent,
    {
        self.as_mut().custom_element(comp);
        self.into()
    }
}
