use wasm_bindgen::UnwrapThrowExt;

pub trait From<T> {
    fn from(item: &T) -> Self;
}

impl<T> From<T> for T
where
    T: Clone,
{
    fn from(item: &T) -> T {
        item.clone()
    }
}

enum ListChangeType {
    NoChange,
    AllNew,
    Push,
    NewItem { index: usize },
    // NewItems { indices: std::vec::Vec<usize> },
    // Extend { start_at: usize },
    ViewChange { index: usize },
    // ViewChanges { indices: std::vec::Vec<usize> },
    Swap { index1: usize, index2: usize },
    Move { old_index: usize, new_index: usize },
    Pop,
    Remove { index: usize },
    Removes { indices: std::vec::Vec<usize> },
    Cleared,
    // Just re-render all items, don't care about the changes
    RerenderAll,
    FallbackDiffing,
}

impl ListChangeType {
    fn relay<'a, T, S, K, F>(
        &self,
        data: &'a [T],
        f: &'a F,
        signal: &'a signals::signal_vec::MutableVec<S>,
    ) where
        T: PartialEq<S>,
        S: Clone + From<T> + Key<Key = K>,
        K: Eq + std::hash::Hash + std::fmt::Debug,
        F: Fn(&T) -> &K,
    {
        match &self {
            ListChangeType::NoChange => {}
            ListChangeType::AllNew => {
                let mut signal = signal.lock_mut();
                signal.clear();
                data.iter()
                    .for_each(|item| signal.push_cloned(S::from(item)));
            }
            ListChangeType::Push => signal
                .lock_mut()
                .push_cloned(S::from(data.last().unwrap_throw())),
            ListChangeType::NewItem { index } => signal
                .lock_mut()
                .insert_cloned(*index, S::from(&data[*index])),
            // ListChangeType::NewItems { indices } => {
            //     let mut signal = signal.lock_mut();
            //     for index in indices {
            //         signal.insert_cloned(*index, S::from(&data[*index]))
            //     }
            // }
            // ListChangeType::Extend { start_at } => {
            //     let mut signal = signal.lock_mut();
            //     data[*start_at..]
            //         .iter()
            //         .for_each(|item| signal.push_cloned(S::from(item)));
            // }
            ListChangeType::ViewChange { index } => {
                signal.lock_mut().set_cloned(*index, S::from(&data[*index]));
            }
            // ListChangeType::ViewChanges { indices } => {
            //     let mut signal = signal.lock_mut();
            //     indices
            //         .iter()
            //         .for_each(|index| signal.set_cloned(*index, S::from(&data[*index])));
            // }
            ListChangeType::Swap { index1, index2 } => {
                let mut signal = signal.lock_mut();
                signal.set_cloned(*index1, S::from(&data[*index1]));
                signal.set_cloned(*index2, S::from(&data[*index2]));
            }
            ListChangeType::Move {
                old_index,
                new_index,
            } => signal.lock_mut().move_from_to(*old_index, *new_index),
            ListChangeType::Pop => {
                signal.lock_mut().pop();
            }
            ListChangeType::Remove { index } => {
                signal.lock_mut().remove(*index);
            }
            ListChangeType::Removes { indices } => {
                let mut signal = signal.lock_mut();
                indices.iter().rev().for_each(|index| {
                    signal.remove(*index);
                });
            }
            ListChangeType::Cleared => {
                signal.lock_mut().clear();
            }
            ListChangeType::RerenderAll => {
                // TODO: Implement this to update the item instead of clearing all and adding back by the `AllNew`
                ListChangeType::AllNew.relay(data, f, signal);
            }
            ListChangeType::FallbackDiffing => match find_diffs(data, f, signal) {
                DiffResult::SimpleChange(change) => change.relay(data, f, signal),
                DiffResult::ComplexChange(cb) => cb.relay(data, signal),
            },
        }
    }
}

#[must_use = "This value should be returned by the `Component::update` method for passing to the Render::update"]
pub struct ListChange(ListChangeType);

impl ListChange {
    pub fn relay<'a, T, S, K, F>(
        &self,
        data: &'a [T],
        f: &'a F,
        signal: &'a signals::signal_vec::MutableVec<S>,
    ) where
        T: PartialEq<S>,
        S: Clone + From<T> + Key<Key = K>,
        K: Eq + std::hash::Hash + std::fmt::Debug,
        F: Fn(&T) -> &K,
    {
        self.0.relay(data, f, signal);
    }

    pub fn no_change() -> Self {
        ListChange(ListChangeType::NoChange)
    }

    pub fn all_new() -> Self {
        ListChange(ListChangeType::AllNew)
    }

    pub fn diffing() -> Self {
        ListChange(ListChangeType::FallbackDiffing)
    }

    pub fn set_no_change(&mut self) {
        self.0 = ListChangeType::NoChange;
    }

    pub fn set_diffing(&mut self) {
        self.0 = ListChangeType::FallbackDiffing;
    }
}

pub trait ListChanges {
    /// Mika passes the output of this to Render::update() on the first render
    fn first_render() -> Self;
    fn diffing() -> Self;
}

impl ListChanges for () {
    fn first_render() -> Self {}
    fn diffing() -> Self {}
}
impl ListChanges for ListChange {
    fn first_render() -> Self {
        ListChange(ListChangeType::AllNew)
    }
    fn diffing() -> Self {
        ListChange(ListChangeType::FallbackDiffing)
    }
}

/// For adding methods to the std::vec::Vec<T> to help relay the change
/// to the Component's Render. Most of its methods just call the equivalent
/// methods from std::vec::Vec<T> and return an appropriate ListChange value.
/// TODO: better names for these methods?
pub trait ListModify<T> {
    fn mika_push(&mut self, item: T) -> ListChange;
    fn mika_pop(&mut self) -> (Option<T>, ListChange);
    fn mika_insert(&mut self, index: usize, item: T) -> ListChange;
    fn mika_swap(&mut self, index1: usize, index2: usize) -> ListChange;
    fn mika_move(&mut self, old_index: usize, new_index: usize) -> ListChange;

    fn mika_get_mut_by<F>(&mut self, f: F) -> Option<(&mut T, ListChange)>
    where
        F: Fn(&T) -> bool;

    fn mika_for_each<F>(&mut self, f: F) -> ListChange
    where
        F: FnMut(&mut T);

    fn mika_retain<F>(&mut self, f: F) -> ListChange
    where
        F: Fn(&T) -> bool;

    fn mika_remove_by<F>(&mut self, f: F) -> Option<(T, ListChange)>
    where
        F: Fn(&T) -> bool;
}

impl<T> ListModify<T> for std::vec::Vec<T> {
    fn mika_push(&mut self, item: T) -> ListChange {
        self.push(item);
        ListChange(ListChangeType::Push)
    }

    fn mika_pop(&mut self) -> (Option<T>, ListChange) {
        (self.pop(), ListChange(ListChangeType::Pop))
    }

    fn mika_insert(&mut self, index: usize, item: T) -> ListChange {
        self.insert(index, item);
        ListChange(ListChangeType::NewItem { index })
    }

    fn mika_swap(&mut self, index1: usize, index2: usize) -> ListChange {
        self.swap(index1, index2);
        ListChange(ListChangeType::Swap { index1, index2 })
    }

    fn mika_move(&mut self, old_index: usize, new_index: usize) -> ListChange {
        let item = self.remove(old_index);
        self.insert(new_index, item);
        ListChange(ListChangeType::Move {
            old_index,
            new_index,
        })
    }

    fn mika_get_mut_by<F>(&mut self, f: F) -> Option<(&mut T, ListChange)>
    where
        F: Fn(&T) -> bool,
    {
        self.iter_mut()
            .enumerate()
            .find(|(_, item)| f(item))
            .map(|(index, item)| (item, ListChange(ListChangeType::ViewChange { index })))
    }

    fn mika_for_each<F>(&mut self, f: F) -> ListChange
    where
        F: FnMut(&mut T),
    {
        self.iter_mut().for_each(f);
        ListChange(ListChangeType::RerenderAll)
    }

    fn mika_retain<F>(&mut self, f: F) -> ListChange
    where
        F: Fn(&T) -> bool,
    {
        // TODO One iteration only?
        let removed_indices = self
            .iter()
            .enumerate()
            .flat_map(|(index, item)| if f(item) { None } else { Some(index) })
            .collect();
        self.retain(f);
        ListChange(ListChangeType::Removes {
            indices: removed_indices,
        })
    }

    fn mika_remove_by<F>(&mut self, f: F) -> Option<(T, ListChange)>
    where
        F: Fn(&T) -> bool,
    {
        match self.iter().position(f) {
            Some(index) => {
                let item = self.remove(index);

                Some((item, ListChange(ListChangeType::Remove { index })))
            }
            None => None,
        }
    }
}

/// A wrapper around std::vec::Vec to prevent you from modifying the inner
/// vec without
/// TODO: better name for this?
#[derive(Default)]
pub struct Vec<T>(std::vec::Vec<T>);

impl<T> std::ops::Deref for Vec<T> {
    type Target = std::vec::Vec<T>;
    fn deref(&self) -> &std::vec::Vec<T> {
        &self.0
    }
}

impl<T> Vec<T> {
    pub fn new() -> Self {
        Vec(std::vec::Vec::new())
    }

    pub fn push(&mut self, item: T) -> ListChange {
        self.0.mika_push(item)
    }

    pub fn pop(&mut self) -> (Option<T>, ListChange) {
        self.0.mika_pop()
    }

    pub fn get_mut_by<F>(&mut self, f: F) -> Option<(&mut T, ListChange)>
    where
        F: Fn(&T) -> bool,
    {
        self.0.mika_get_mut_by(f)
    }

    pub fn for_each<F>(&mut self, f: F) -> ListChange
    where
        F: FnMut(&mut T),
    {
        self.0.mika_for_each(f)
    }

    pub fn retain<F>(&mut self, f: F) -> ListChange
    where
        F: Fn(&T) -> bool,
    {
        self.0.mika_retain(f)
    }

    pub fn remove_by<F>(&mut self, f: F) -> Option<(T, ListChange)>
    where
        F: Fn(&T) -> bool,
    {
        self.0.mika_remove_by(f)
    }

    pub fn item_view_change<F>(&mut self, f: F) -> ListChange
    where
        F: Fn(&T) -> bool,
    {
        match self.0.iter().enumerate().find(|(_, item)| f(item)) {
            Some((index, _)) => ListChange(ListChangeType::ViewChange { index }),
            None => ListChange(ListChangeType::NoChange),
        }
    }
}

pub trait Key {
    type Key: Eq + std::hash::Hash;
    fn key(&self) -> &Self::Key;
}

pub trait KeyedItem<C: crate::app::Component>: Key {
    type RenderOutput: crate::dom::traits::CloneWsElement;
    type ItemsNeedUpdate;

    fn template() -> Self::RenderOutput;

    fn render(
        &self,
        comp: &crate::app::CompHandle<C>,
        template: Self::RenderOutput,
    ) -> (Self::RenderOutput, Self::ItemsNeedUpdate);

    fn update(
        &self,
        comp: &crate::app::CompHandle<C>,
        element: &mut Self::RenderOutput,
        to_update: &mut Self::ItemsNeedUpdate,
    );
}

pub struct LisDiffChanges {
    removals: std::vec::Vec<usize>,
    other_changes: std::vec::Vec<Patch>,
}

// Just store index for now because of https://github.com/axelf4/lis/issues/1
#[derive(Debug)]
enum Patch {
    Moved {
        old_index: usize,
        new_index: usize,
        updated: bool,
    },
    Inserted {
        new_index: usize,
    },
    // Reported as `unchanged` by `lis` (because it only compare by key)
    // but the item value changed
    Updated {
        old_index: usize,
        new_index: usize,
    },
}

impl<T, S> lis::DiffCallback<(usize, &S), (usize, &T)> for LisDiffChanges
where
    T: PartialEq<S>,
{
    fn inserted(&mut self, (new_index, _): (usize, &T)) {
        self.other_changes.push(Patch::Inserted { new_index });
    }

    fn removed(&mut self, (old_index, _): (usize, &S)) {
        self.removals.push(old_index);
    }

    fn unchanged(&mut self, (old_index, old): (usize, &S), (new_index, new): (usize, &T)) {
        if new != old {
            self.other_changes.push(Patch::Updated {
                old_index,
                new_index,
            });
        }
    }

    fn moved(&mut self, (old_index, old): (usize, &S), (new_index, new): (usize, &T)) {
        self.other_changes.push(Patch::Moved {
            updated: new != old,
            old_index,
            new_index,
        });
    }
}

enum DiffResult {
    SimpleChange(ListChangeType),
    ComplexChange(LisDiffChanges),
}

fn find_diffs<'a, T, S, K, F>(
    new: &'a [T],
    f: &F,
    old: &'a signals::signal_vec::MutableVec<S>,
) -> DiffResult
where
    T: PartialEq<S>,
    S: Clone + From<T> + Key<Key = K>,
    K: Eq + std::hash::Hash + std::fmt::Debug,
    F: Fn(&T) -> &K,
{
    if new.is_empty() {
        return DiffResult::SimpleChange(ListChangeType::Cleared);
    }

    let locked_old = old.lock_ref();
    if locked_old.is_empty() {
        return DiffResult::SimpleChange(ListChangeType::AllNew);
    }

    let old_len = locked_old.len();

    let mut cb = LisDiffChanges {
        // TODO Cache these Vecs for reuse to avoid allocation.
        removals: std::vec::Vec::with_capacity(old_len),
        other_changes: std::vec::Vec::with_capacity(new.len().max(old_len)),
    };

    lis::diff_by_key(
        locked_old.iter().enumerate(),
        |x| x.1.key(),
        new.iter().enumerate(),
        |x| f(x.1),
        &mut cb,
    );
    match (cb.removals.len(), cb.other_changes.len()) {
        (0, 0) => DiffResult::SimpleChange(ListChangeType::NoChange),
        (remove_count, 0) => {
            if remove_count == old_len {
                DiffResult::SimpleChange(ListChangeType::AllNew)
            } else {
                DiffResult::SimpleChange(ListChangeType::Removes {
                    indices: cb.removals,
                })
            }
        }
        _ => DiffResult::ComplexChange(cb),
    }
}

impl LisDiffChanges {
    fn relay<'a, T, S>(self, new: &'a [T], old: &'a signals::signal_vec::MutableVec<S>)
    where
        S: Clone + From<T>,
    {
        let mut old = old.lock_mut();
        let mut removal_shifts: std::vec::Vec<usize> = vec![0; old.len()];

        for removed in self.removals.into_iter().rev() {
            old.remove(removed);
            removal_shifts[removed] = 1;
        }

        let mut shift = 0;
        for removal_shift in removal_shifts.iter_mut() {
            if *removal_shift == 1 {
                shift += 1;
            }
            *removal_shift = shift;
        }

        let mut insert_shift = 0;
        for p in self.other_changes.into_iter().rev() {
            match p {
                Patch::Moved {
                    old_index,
                    new_index,
                    updated,
                } => {
                    let actual_index = old_index + insert_shift - removal_shifts[old_index];
                    old.move_from_to(actual_index, new_index);
                    if updated {
                        old.set_cloned(
                            new_index,
                            S::from(new.get(new_index).expect_throw("Patch::Moved")),
                        );
                    }
                    insert_shift += 1;
                }
                Patch::Inserted { new_index } => {
                    old.insert_cloned(
                        new_index,
                        S::from(new.get(new_index).expect_throw("Patch::Inserted")),
                    );
                    insert_shift += 1;
                }
                Patch::Updated {
                    old_index,
                    new_index,
                } => {
                    // TODO: Is this actual_index always the same value as old_index?
                    let actual_index = old_index + insert_shift - removal_shifts[old_index];
                    old.set_cloned(
                        actual_index,
                        S::from(new.get(new_index).expect_throw("Patch::Updated")),
                    );
                }
            }
        }
    }
}

mod tests {
    impl super::Key for i32 {
        type Key = i32;
        fn key(&self) -> &Self::Key {
            self
        }
    }

    #[test]
    fn relay_list_change_by_diffing() {
        // Start as an empty list
        let signal = signals::signal_vec::MutableVec::<i32>::new();

        // Some new items
        let vec: Vec<i32> = vec![1, 3, 5];
        let change = super::ListChangeType::FallbackDiffing;
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // More insertions
        let vec: Vec<i32> = vec![0, 1, 2, 3, 4, 5];
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Removals, insertions, and moves
        let vec: Vec<i32> = vec![5, 7, 3, 8, 2, 1, 6];
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // More removals, insertions, and moves
        let vec: Vec<i32> = vec![6, 5, 4, 9, 3, 8, 0, 2];
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Removals only
        let vec: Vec<i32> = vec![6, 5, 4, 9, 8, 0, 2];
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Remove everything
        let vec: Vec<i32> = Vec::new();
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);
    }

    #[test]
    fn relay_list_change() {
        // Start as an empty list
        let signal = signals::signal_vec::MutableVec::<i32>::new();

        // Some new items
        let vec: Vec<i32> = vec![1, 3, 5];
        super::ListChangeType::AllNew.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Insertion
        let vec: Vec<i32> = vec![0, 1, 3, 5];
        let change = super::ListChangeType::NewItem { index: 0 };
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Insertion
        let vec: Vec<i32> = vec![0, 1, 2, 3, 5];
        let change = super::ListChangeType::NewItem { index: 2 };
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Insertion
        let vec: Vec<i32> = vec![0, 1, 2, 3, 4, 5];
        let change = super::ListChangeType::NewItem { index: 4 };
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Move
        let vec: Vec<i32> = vec![1, 2, 3, 4, 5, 0];
        let change = super::ListChangeType::Move {
            old_index: 0,
            new_index: 5,
        };
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Move back
        let vec: Vec<i32> = vec![0, 1, 2, 3, 4, 5];
        let change = super::ListChangeType::Move {
            old_index: 5,
            new_index: 0,
        };
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Move middle item
        let vec: Vec<i32> = vec![0, 2, 3, 1, 4, 5];
        let change = super::ListChangeType::Move {
            old_index: 1,
            new_index: 3,
        };
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Move back
        let vec: Vec<i32> = vec![0, 1, 2, 3, 4, 5];
        let change = super::ListChangeType::Move {
            old_index: 3,
            new_index: 1,
        };
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Swap
        let vec: Vec<i32> = vec![0, 5, 2, 3, 4, 1];
        let change = super::ListChangeType::Swap {
            index1: 1,
            index2: 5,
        };
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Pop
        let vec: Vec<i32> = vec![0, 5, 2, 3, 4];
        let change = super::ListChangeType::Pop;
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Removes
        let vec: Vec<i32> = vec![5, 3, 4];
        let change = super::ListChangeType::Removes {
            indices: vec![0, 2],
        };
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Remove
        let vec: Vec<i32> = vec![5, 4];
        let change = super::ListChangeType::Remove { index: 1 };
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Push
        let vec: Vec<i32> = vec![5, 4, 7];
        let change = super::ListChangeType::Push;
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);

        // Clear
        let vec: Vec<i32> = Vec::new();
        let change = super::ListChangeType::Cleared;
        change.relay(&vec, &|x| x, &signal);
        assert_eq!(vec[..], signal.lock_ref()[..]);
    }
}
