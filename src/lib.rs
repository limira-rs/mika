//! # `Mika`
//! *A signal-based framework for building front-end app, it tries to help, but may cause annoyances*.
//! You can find [the Guide here](https://limira-rs.gitlab.io/mika/)
mod app;
#[macro_use]
pub mod macros;
pub mod callbacks;
pub mod dom;
mod errors;
pub mod events;
pub mod fetch;
mod list;
mod spawn;

use wasm_bindgen::UnwrapThrowExt;

pub mod prelude {
    pub use crate::app::{Component, DefaultComponent};
    pub use crate::dom::traits::{
        AllowComponent, AllowText, FlowContent, GlobalAttributes, HasWsElement, HasWsNode,
    };
    pub use crate::list::{Key, ListChange, ListChanges, ListModify};
}

pub fn window() -> web_sys::Window {
    web_sys::window().expect_throw("root::window: web_sys::window()")
}

pub fn document() -> web_sys::Document {
    crate::window()
        .document()
        .expect_throw("root::document: crate::window().document()")
}

// Reexport from web-sys
pub use web_sys::Node as WsNode;

pub use app::{
    App, Commands, Comp, CompHandle, Component, Context, CustomElement, RawApp, RawComponent,
};
pub use errors::Error;
pub use list::{From, Key, KeyedItem, ListChange, Vec};
pub use mika_macros::convert_to_impl_keyed_item;
pub use spawn::SignalCenter;
