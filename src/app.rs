use wasm_bindgen::UnwrapThrowExt;

use crate::dom::traits::HasWsNode;
use crate::list::ListChanges;

/// For defining a raw-component that users deal with the signals themselves. With
/// components of this types, users must deal with signals all the time (from the
/// construction of app state, working with the app state, to the rendering of views
/// from signals). To work with signals, a user have to break their data apart and
/// wrap them in `Mutable<T>`, or `MutableVec<T>`... And if the user want to send data
/// over the network, they must collect `T` from `Mutable<T>`, or `Vec<T>` from
/// `MutableVec<T>` before doing the serialization. It is a bit annoyed working like
/// this, so may want to use [Component](trait.Component.html) instead.
///
/// Although `RawComponent` contains the word "raw" in its name, it is not depended
/// on by the `Component` trait, they are unrelated traits.
pub trait RawComponent {
    type RenderOutput: HasWsNode + Into<crate::dom::TotalElement>;
    fn init(_app: &std::rc::Rc<Self>) {}
    fn render(app: &std::rc::Rc<Self>) -> Self::RenderOutput;
    fn mounted(_app: &std::rc::Rc<Self>) {}
}

/// To run an app with the main component is a RawComponent
pub struct RawApp<C: RawComponent> {
    _app: std::rc::Rc<C>,
    _dom: crate::dom::Node,
}

impl<C, E> RawApp<C>
where
    C: RawComponent<RenderOutput = E>,
    E: HasWsNode + crate::dom::MountableToBody + Into<crate::dom::TotalElement>,
{
    pub fn mount_to_body(app: C) -> Self {
        let app = std::rc::Rc::new(app);
        C::init(&app);
        let dom = C::render(&app);
        if !dom.is_body() {
            let body = crate::document()
                .body()
                .expect_throw("RawApp::mount_to_body: crate::document().body()");
            body.set_text_content(None);
            dom.append_to(body.as_ref());
        }
        C::mounted(&app);
        Self {
            _app: app,
            _dom: dom.into().into(),
        }
    }
}

impl<C, E> RawApp<C>
where
    C: RawComponent<RenderOutput = E>,
    E: crate::dom::traits::FlowContent + Into<crate::dom::TotalElement>,
{
    pub fn mount_to(app: C, div: &crate::dom::Div) -> Self {
        let app = std::rc::Rc::new(app);
        C::init(&app);
        let dom = C::render(&app);
        div.ws_node().set_text_content(None);
        dom.append_to(div.ws_node());
        C::mounted(&app);
        Self {
            _app: app,
            _dom: dom.into().into(),
        }
    }
}

pub trait FlowContentRawComponent {
    fn init(&self);
    fn render_in(&self, node: &web_sys::Node) -> crate::dom::TotalElement;
    fn render_and_replace(
        &self,
        parent_node: &web_sys::Node,
        to_be_replaced: &mut web_sys::Node,
    ) -> crate::dom::TotalElement;
    fn mounted(&self);
}

impl<C, E> FlowContentRawComponent for std::rc::Rc<C>
where
    C: RawComponent<RenderOutput = E>,
    E: crate::dom::traits::FlowContent + Into<crate::dom::TotalElement>,
{
    fn init(&self) {
        C::init(self);
    }
    fn render_in(&self, node: &web_sys::Node) -> crate::dom::TotalElement {
        let dom = C::render(self);
        dom.append_to(node);
        dom.into()
    }
    fn render_and_replace(
        &self,
        parent_node: &web_sys::Node,
        to_be_replaced: &mut web_sys::Node,
    ) -> crate::dom::TotalElement {
        let dom = C::render(self);
        dom.replace(parent_node, to_be_replaced);
        *to_be_replaced = dom.ws_node().clone();
        dom.into()
    }
    fn mounted(&self) {
        C::mounted(self)
    }
}

impl<C, E> From<C> for Box<dyn FlowContentRawComponent>
where
    C: 'static + RawComponent<RenderOutput = E>,
    E: crate::dom::traits::FlowContent + Into<crate::dom::TotalElement>,
{
    fn from(item: C) -> Self {
        Box::new(std::rc::Rc::new(item))
    }
}

impl<C, E> From<std::rc::Rc<C>> for Box<dyn FlowContentRawComponent>
where
    C: 'static + RawComponent<RenderOutput = E>,
    E: crate::dom::traits::FlowContent + Into<crate::dom::TotalElement>,
{
    fn from(item: std::rc::Rc<C>) -> Self {
        Box::new(item)
    }
}

/// Supports for high level components that doesn't contain any signals.
/// Their states just contain normal data, potentially just exactly what
/// is received (and parsed) from the network. Components of this type
/// do not render the view. They require a `Render` to do the render.
/// The `Render` comprises of signals that must be updated every time
/// the component state changes.
///
/// Although there is a [RawComponent](trait.RawComponent.html), Component trait does not
/// depend on it. They are unrelated traits.
pub trait Component: 'static + Default {
    type State;
    type ListChange: crate::list::ListChanges;
    type Output: HasWsNode + Into<crate::dom::TotalElement>;

    // TODO a better name?
    fn new(state: Self::State) -> Comp<Self> {
        Comp::new(state)
    }

    fn pre_render(_comp: &CompHandle<Self>) -> Option<Vec<Box<dyn crate::events::Listener>>> {
        None
    }

    /// This method is executed immediately after the component is attached
    /// to its parent node. A node is only attached to the its parent after
    /// its content is fully built. When a child component is attached to its
    /// parent, the parent node is not completed its build yet, therefore, when
    /// `Component::post_render` is executed, the component's node is not
    /// attach to the DOM.
    ///
    /// For the main component of the app, this is equivalent to  `after mounted`
    /// (the component is already mounted to the DOM).
    fn post_render(&self, _commands: &mut Commands<Self>) {}
    fn diff(&self, state: &Self::State, list_change: Self::ListChange);

    /// Executed only once when the app start or the component is first created
    /// by its parent.
    fn render(&self, comp: &CompHandle<Self>) -> Self::Output;
}

pub trait DefaultComponent: Component {
    // TODO a better name?
    fn default_state() -> Comp<Self>;
}

impl<C> DefaultComponent for C
where
    C: Component,
    C::State: Default,
{
    fn default_state() -> Comp<Self> {
        Comp::new(Self::State::default())
    }
}

pub struct Context<C: Component> {
    pub update_view: bool,
    pub list_change: C::ListChange,
    pub commands: Commands<C>,
}

impl<C: Component> From<()> for Context<C> {
    fn from(_: ()) -> Self {
        Context::<C>::default()
    }
}

impl<C: 'static + Component> Default for Context<C> {
    fn default() -> Self {
        Self {
            update_view: true,
            list_change: C::ListChange::diffing(),
            commands: Commands::new(),
        }
    }
}

pub trait Command<C: Component> {
    // I want this take `self`, but it does not work inside a Box.
    // A `&mut self` cause implementors have to hack with `Option<T>`
    // to take ownership of data they want.
    fn execute(&mut self, comp: &CompHandle<C>);
}

pub struct Commands<C: Component> {
    cmds: Vec<Box<dyn Command<C>>>,
}

impl<C> Commands<C>
where
    C: 'static + Component,
{
    fn new() -> Self {
        Self { cmds: Vec::new() }
    }

    pub fn push<C2: 'static + Command<C>>(&mut self, cmd: C2) {
        self.cmds.push(Box::new(cmd));
    }

    fn execute(&mut self, comp: &CompHandle<C>) {
        self.cmds.iter_mut().for_each(|cmd| cmd.execute(comp));
    }

    pub fn fetch_json_ok_error2<T>(
        &mut self,
        req: crate::fetch::Request,
        ok_handler: fn(&mut C::State, T),
        error_handler: fn(&mut C::State, crate::Error),
    ) where
        T: 'static + serde::de::DeserializeOwned,
    {
        self.push(crate::fetch::FetchCommand::new(
            req,
            ok_handler,
            error_handler,
        ))
    }
}

/// To run an app with the main component is a Component
pub struct App<C: Component> {
    _comp_handle: CompHandle<C>,
    _rendered_dom: crate::dom::Node,
    _events: Option<Vec<Box<dyn crate::events::Listener>>>,
}

impl<C, S, E> App<C>
where
    C: 'static + Component<State = S, Output = E>,
    S: Default,
    E: HasWsNode + crate::dom::MountableToBody + Into<crate::dom::TotalElement>,
{
    pub fn mount_to_body(comp_state: C::State) -> Self {
        let comp_handle = CompHandle::new(comp_state);
        let events = C::pre_render(&comp_handle);
        let dom = comp_handle.first_render();
        if !dom.is_body() {
            let body = crate::document()
                .body()
                .expect_throw("RawApp::mount_to_body: crate::document().body()");
            body.set_text_content(None);
            dom.append_to(body.as_ref());
        }
        comp_handle.post_render();

        Self {
            _comp_handle: comp_handle,
            _rendered_dom: dom.into().into(),
            _events: events,
        }
    }
}

impl<C, S, E> App<C>
where
    C: 'static + Component<State = S, Output = E>,
    S: Default,
    E: crate::dom::traits::FlowContent + Into<crate::dom::TotalElement>,
{
    pub fn mount_to(comp_state: C::State, div: &crate::dom::Div) -> Self {
        let comp_handle = CompHandle::new(comp_state);
        let events = C::pre_render(&comp_handle);
        let dom = comp_handle.first_render();
        div.ws_node().set_text_content(None);
        dom.append_to(div.ws_node());
        comp_handle.post_render();

        Self {
            _comp_handle: comp_handle,
            _rendered_dom: dom.into().into(),
            _events: events,
        }
    }
}

pub struct CompHandle<C: Component> {
    comp_state: std::rc::Rc<std::cell::RefCell<C::State>>,
    renderer: std::rc::Rc<C>,
}

impl<C: Component> Clone for CompHandle<C> {
    fn clone(&self) -> Self {
        Self {
            comp_state: self.comp_state.clone(),
            renderer: self.renderer.clone(),
        }
    }
}

impl<C: 'static + Component> CompHandle<C> {
    fn new(comp_state: C::State) -> Self {
        let renderer = std::rc::Rc::new(C::default());
        CompHandle {
            comp_state: std::rc::Rc::new(std::cell::RefCell::new(comp_state)),
            renderer,
        }
    }

    /// Does this cause any problems?
    pub fn state(&self) -> std::cell::Ref<C::State> {
        self.comp_state.borrow()
    }

    pub fn render(&self) -> &std::rc::Rc<C> {
        &self.renderer
    }

    fn first_render(&self) -> C::Output {
        self.renderer
            .diff(&self.comp_state.borrow(), C::ListChange::first_render());
        self.renderer.render(self)
    }

    fn post_render(&self) {
        let mut commands = Commands::new();
        self.renderer.post_render(&mut commands);
        commands.execute(self);
    }

    fn after_update(&self, context: Context<C>) {
        let Context {
            update_view,
            list_change,
            mut commands,
        } = context;
        if update_view {
            self.renderer.diff(&self.comp_state.borrow(), list_change);
        }
        commands.execute(&self);
    }

    pub fn update<Ct: Into<Context<C>>>(&self, updater: &impl Fn(&mut C::State) -> Ct) {
        let context = match self.comp_state.try_borrow_mut() {
            Ok(ref mut comp_state) => {
                updater(comp_state)
                    // This create a default value of Context<C> if `updater` returns `()`.
                    // Should we check the type id of `()` instead of this?
                    .into()
            }
            Err(e) => {
                log::error!("Update error: {}", e);
                return;
            }
        };
        self.after_update(context);
    }

    pub fn update_with_arg<T, Ct: Into<Context<C>>>(&self, value: T, updater: &impl Fn(&mut C::State, T) -> Ct) {
        let context = match self.comp_state.try_borrow_mut() {
            Ok(ref mut comp_state) => {
                updater(comp_state, value)
                    // Similar comment in `update`
                    .into()
            }
            Err(e) => {
                log::error!("Update error: {}", e);
                return;
            }
        };
        self.after_update(context);
    }

    /*pub fn update_with_context(&self, updater: &impl Fn(&mut C::State, &mut Context<C>)) {
        match self.comp_state.try_borrow_mut() {
            Ok(ref mut comp_state) => {
                // TODO: Create a new context everytime or create one (store in self)
                // and reset them before every update?
                let mut context = Context::default();
                updater(comp_state, &mut context);
                let Context {
                    update_view,
                    list_change,
                    mut commands,
                } = context;
                commands.execute(self);
                if update_view {
                    self.renderer.diff(&comp_state, list_change);
                }
            }
            Err(e) => log::error!("Update error: {}", e),
        }
    }

    pub fn update_with_arg_and_context<T>(
        &self,
        value: T,
        updater: &impl Fn(&mut C::State, T, &mut Context<C>),
    ) {
        match self.comp_state.try_borrow_mut() {
            Ok(ref mut comp_state) => {
                // TODO: Create a new context everytime or create one (store in self)
                // and reset them before every update?
                let mut context = Context::default();
                updater(comp_state, value, &mut context);
                let Context {
                    update_view,
                    list_change,
                    mut commands,
                } = context;
                commands.execute(self);
                if update_view {
                    self.renderer.diff(&comp_state, list_change);
                }
            }
            Err(e) => log::error!("Update error: {}", e),
        }
    }*/
}

pub struct Comp<C: Component> {
    comp_handle: CompHandle<C>,
    future_handles: Vec<discard::DiscardOnDrop<signals::CancelableFutureHandle>>,
    _events: Option<Vec<Box<dyn crate::events::Listener>>>,
}

impl<C: 'static + Component> Comp<C> {
    pub fn new(comp: C::State) -> Self {
        Self {
            comp_handle: CompHandle::new(comp),
            future_handles: Vec::new(),
            _events: None,
        }
    }

    pub fn update_signal<F, S>(&mut self, signal: S)
    where
        F: 'static + Fn(&mut C::State),
        S: 'static + signals::signal::Signal<Item = F>,
    {
        let comp_handle = self.comp_handle.clone();
        let handle = crate::spawn::for_each(signal, move |updater| {
            comp_handle.update(&updater);
        });
        self.future_handles.push(handle);
    }
}

pub trait FlowContentComponent {
    fn pre_render(&mut self);
    fn render_in(&self, node: &web_sys::Node) -> crate::dom::TotalElement;
    fn render_and_replace(
        &self,
        parent_node: &web_sys::Node,
        to_be_replaced: &mut web_sys::Node,
    ) -> crate::dom::TotalElement;
    fn post_render(&self);
}

impl<C, E> FlowContentComponent for Comp<C>
where
    C: 'static + Component<Output = E>,
    E: crate::dom::traits::FlowContent + Into<crate::dom::TotalElement>,
{
    fn pre_render(&mut self) {
        self._events = C::pre_render(&self.comp_handle);
    }
    fn render_in(&self, node: &web_sys::Node) -> crate::dom::TotalElement {
        //let dom = R::render(&self.comp_handle.renderer, &self.comp_handle);
        let dom = self.comp_handle.first_render();
        dom.append_to(node);
        dom.into()
    }
    fn render_and_replace(
        &self,
        parent_node: &web_sys::Node,
        to_be_replaced: &mut web_sys::Node,
    ) -> crate::dom::TotalElement {
        //let dom = R::render(&self.comp_handle.renderer, &self.comp_handle);
        let dom = self.comp_handle.first_render();
        dom.replace(parent_node, to_be_replaced);
        *to_be_replaced = dom.ws_node().clone();
        dom.into()
    }
    fn post_render(&self) {
        self.comp_handle.post_render();
    }
}

impl<C, E> From<Comp<C>> for Box<dyn FlowContentComponent>
where
    C: 'static + Component<Output = E>,
    E: crate::dom::traits::FlowContent + Into<crate::dom::TotalElement>,
{
    fn from(item: Comp<C>) -> Self {
        Box::new(item)
    }
}

/// CustomElement is intended for wrapping an element that (its content) is rendered by a
/// JavaScript library. It is not related to https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements
/// ## Example
/// ```rust
/// struct JsWrapper {
///     container: mika::dom::Div,
///     js_lib: some_path::ImportedTypeFromJs,
/// }
/// impl JsWrapper {
///     fn new() -> Self {
///         let container = mika::dom::Div::new();
///
///         // Get the `web_sys::Element` from the `container` and pass it to the
///         // ImportedTypeFromJs
///         let js_lib = some_path::ImportedTypeFromJs::new(container.websys_element());
///
///         Self {
///             container,
///             js_lib,
///         }
///     }
/// }
/// impl mika::CustomElement {
///     type Container = mika::dom::Div;
///     fn container(&self) -> &Self::Container {
///         &self.container
///     }
///     fn mounted(&self) {
///         // Execute some method of ImportedTypeFromJs
///         self.js_lib.do_something();
///     }
/// }
/// // Finally, in the Render
/// impl mika::Render<YourComp> for Render {
///     ...
///     fn render(self: std::rc::Rc<Self>, comp: &mika::CompHandle<YourComp>) -> Self::Output {
///         mika::dom::Div().new()
///             .stateless_component(JsWrapper::new())
///     }
/// }
/// ```
pub trait CustomElement {
    type Container: crate::dom::traits::FlowContent;
    fn container(&self) -> &Self::Container;
    fn mounted(&self) {}
}

pub trait FlowContentCustomElement {
    fn container_ws_node(&self) -> &web_sys::Node;
    fn mounted(&self);
}

impl<C, E> FlowContentCustomElement for C
where
    C: CustomElement<Container = E>,
    E: 'static + crate::dom::traits::FlowContent,
{
    fn container_ws_node(&self) -> &web_sys::Node {
        self.container().ws_node()
    }
    fn mounted(&self) {
        self.mounted();
    }
}

impl HasWsNode for Box<dyn FlowContentCustomElement> {
    fn ws_node(&self) -> &web_sys::Node {
        self.container_ws_node()
    }
}
