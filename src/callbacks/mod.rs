//! Create closures that send messages back to a Component or execute methods
//! of a RawComponent

pub mod event_handlers;
pub mod js;
pub mod raw;

pub fn callback_no_arg<C, F, Ct>(comp_handle: &crate::app::CompHandle<C>, f: F) -> impl Fn()
where
    C: 'static + crate::app::Component,
    F: Fn(&mut C::State) -> Ct,
    Ct: Into<crate::app::Context<C>>,
{
    let comp_handle = comp_handle.clone();
    move || comp_handle.update(&f)
}

pub fn callback_arg<C, F, T, Ct>(comp_handle: &crate::app::CompHandle<C>, f: F) -> impl Fn(T)
where
    C: 'static + crate::app::Component,
    F: Fn(&mut C::State, T) -> Ct,
    Ct: Into<crate::app::Context<C>>,
{
    let comp_handle = comp_handle.clone();
    move |arg: T| comp_handle.update_with_arg(arg, &f)
}

// pub fn callback_context<C, F>(comp_handle: &crate::app::CompHandle<C>, f: F) -> impl Fn()
// where
//     C: 'static + crate::app::Component,
//     F: Fn(&mut C::State, &mut crate::app::Context<C>),
// {
//     let comp_handle = comp_handle.clone();
//     move || comp_handle.update_with_context(&f)
// }

// pub fn callback_arg_context<C, F, T>(comp_handle: &crate::app::CompHandle<C>, f: F) -> impl Fn(T)
// where
//     C: 'static + crate::app::Component,
//     F: Fn(&mut C::State, T, &mut crate::app::Context<C>),
// {
//     let comp_handle = comp_handle.clone();
//     move |arg: T| comp_handle.update_with_arg_and_context(arg, &f)
// }
