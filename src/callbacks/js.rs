//! Help creating closures that send messages from JavaScript code back to Component

// /// Create a wasm-bindgen Closure that you can pass to JavaScript for sending message back to the Component's update
// pub fn no_arg<C, F>(
//     comp: &crate::app::CompHandle<C>,
//     handler: F,
// ) -> wasm_bindgen::closure::Closure<dyn Fn()>
// where
//     C: 'static + crate::app::Component,
//     F: 'static + Fn() -> C::Message,
// {
//     let comp = comp.clone();
//     let handler = move || {
//         comp.update(handler());
//     };
//     wasm_bindgen::closure::Closure::wrap(Box::new(handler) as Box<dyn Fn()>)
// }

// /// Create a wasm-bindgen Closure that you can pass to JavaScript. The caller pass an object that will be
// /// parsed into a Rust type `T`. If the parsing is successful, it will send to Component via `ok_handler`,
// /// otherwise, an error will send to Component via error_handler.
// pub fn serde_ok_error<C, T, F1, F2>(
//     comp: &crate::app::CompHandle<C>,
//     ok_handler: F1,
//     error_handler: F2,
// ) -> wasm_bindgen::closure::Closure<dyn Fn(wasm_bindgen::JsValue)>
// where
//     C: 'static + crate::app::Component,
//     T: serde::de::DeserializeOwned,
//     F1: 'static + Fn(T) -> C::Message,
//     F2: 'static + Fn(crate::Error) -> C::Message,
// {
//     let comp = comp.clone();
//     let handler = move |value: wasm_bindgen::JsValue| {
//         let value: Result<T, serde_json::Error> = value.into_serde();
//         match value {
//             Ok(data) => comp.update(ok_handler(data)),
//             Err(e) => comp.update(error_handler(From::from(e))),
//         }
//     };
//     wasm_bindgen::closure::Closure::wrap(Box::new(handler) as Box<dyn Fn(wasm_bindgen::JsValue)>)
// }
