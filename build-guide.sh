#!/bin/bash

set -e

# This line force the `build.rs` run on every build.
touch ./guide/content

wasm-pack build --debug --out-dir ../mika --target web --no-typescript ./guide
cp -u ./guide/static/* ./mika/

case "$1" in 
    "serve") python3 -m http.server;;
    *) echo "Build done! Run './build-guide.sh serve' to serve at 'http://localhost:8000/'";;
esac