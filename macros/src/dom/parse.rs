//! Parse html template with the following syntax:
//!
//! ItemList        : (Item)*
//!
//! Item            : HtmlElement | LiteralText | SimpleExpressionAsText
//!
//!
//! // Not consider special case `<hr>` and `<br>` yet, for now it works as `hr()`/`br()`
//!
//! HtmlElement     :  | html_tag { ItemList }
//!                     | html_tag ((attribute)*)
//!                     | html_tag ((attribute)*) { ItemList }
//!
//! html_tag        : div | span | input | button | ...
//! LiteralText     : "..."
//!
//! SimpleExpressionAsText :
//!                 | variable_ident
//!                 | path::to::item::CONST
//!                 | a::function::call()
//!
//! attribute       : Rust-string-literal | key=value
//!
//! key             : Rust-identifer
//! value           : Rust-expression

use super::*;
use crate::helper::ParseExt;
use syn::ext::IdentExt;

impl syn::parse::Parse for KeyedItemTemplate {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        Ok(KeyedItemTemplate {
            component_name: input.parse_at_component_name()?,
            init_block: input.parse_optional_brace()?,
            html_element: input.parse()?,
        })
    }
}
impl syn::parse::Parse for ItemList {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let mut items: Vec<Item> = Vec::new();
        while !input.is_empty() {
            items.push(input.parse()?);
        }
        Ok(Self(items))
    }
}

impl syn::parse::Parse for Item {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let constant = input.parse_constant_symbol()?;
        let item = if input.peek(syn::Ident::peek_any) {
            // TODO: Check if the `peek`ed ident is actually an html element.
            // Without checking for valid html tag now, all function call `some_name()` will
            // be parsed as an HtmlElement. (Method calls such as `self.some_name()`... will
            // be correctly parsed as text expressions)
            if input.peek2(syn::token::Brace) || input.peek2(syn::token::Paren) {
                Item::HtmlElement(input.parse()?)
            } else if constant {
                Item::TextSimpleExpressionAsConstant(input.parse()?)
            } else if input.peek_signal_symbol() {
                input.parse_signal_symbol()?; // just discard it
                Item::TextSignal(input.parse()?)
            } else {
                Item::TextSimpleExpression(input.parse()?)
            }
        } else if input.peek(syn::LitStr) {
            Item::TextLiteralStr(input.parse()?)
        } else if input.peek(syn::Lit) {
            Item::TextOtherLiteral(input.parse()?)
        } else {
            return Err(input.error(""));
        };
        Ok(item)
    }
}

impl syn::parse::Parse for HtmlElement {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let tag: syn::Ident = input.parse()?;
        let attributes = input
            .parse_optional_paren()?
            .unwrap_or_else(Attributes::default);
        let child_items = input
            .parse_optional_brace()?
            .unwrap_or_else(ItemList::default);
        Ok(HtmlElement {
            tag,
            attributes,
            child_items,
        })
    }
}
impl syn::parse::Parse for Attributes {
    // TODO: Check for valid values by looking up at `super::TYPE_INFOS`
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let mut attributes = Attributes::default();
        while !input.is_empty() {
            if input.peek(syn::LitStr) {
                // For syntax like:
                //      "standalone-class-name" or
                //      "conditional-class-name"=some.bool.expression()

                let class_name: syn::LitStr = input.parse()?;
                if input.peek(syn::token::Eq) {
                    let _: syn::token::Eq = input.parse()?;
                    attributes.conditional_classes.push(ConditionalClass {
                        class_name,
                        by_signal: input.parse_signal_symbol()?,
                        bool_expression: input.parse()?,
                    });
                } else {
                    attributes.literal_classes.push(class_name);
                }
            } else {
                // All other attributes with syntax:
                //      key=value
                // where
                //      key: syn::Ident
                //      value: syn::Expr | syn::LitStr

                // Determine a `rust_keyword` by this is generally not correct.
                // But we only proceed if it is an ident, then still correct in this context?
                let rust_keyword = !input.peek(syn::Ident);
                let name = input.call(syn::Ident::parse_any)?;

                let _eq: syn::token::Eq = input.parse()?;
                if name == "class" {
                    attributes.literal_classes.push(input.parse()?);
                } else if super::is_html_event(&name) {
                    attributes.events.push(ElementEvent {
                        name,
                        message: input.parse()?,
                    });
                } else {
                    attributes.attributes.push(Attribute {
                        name,
                        rust_keyword,
                        by_signal: input.parse_signal_symbol()?,
                        value: input.parse()?,
                    })
                }
            }
        }
        Ok(attributes)
    }
}

impl syn::parse::Parse for AttributeValue {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let av = if input.peek(syn::Lit) {
            AttributeValue::Lit(input.parse()?)
        } else {
            AttributeValue::Expr(input.parse()?)
        };
        Ok(av)
    }
}

impl syn::parse::Parse for ElementEventMessage {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        Ok(ElementEventMessage {
            path: input.parse()?,
            args: input.parse_optional_paren()?,
        })
    }
}

impl syn::parse::Parse for ElementEventMessageArgs {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let place_holder = input.parse_placeholder_symbol()?;
        let args: proc_macro2::TokenStream = input.parse()?;
        Ok(ElementEventMessageArgs {
            receive_js_argument: place_holder,
            args,
        })
    }
}
