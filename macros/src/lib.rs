extern crate proc_macro;

mod convert_to_impl_keyed_item;
// mod dom;
mod helper;
mod keywords;

// #[proc_macro_attribute]
// pub fn keyed_item(
//     attr: proc_macro::TokenStream,
//     input: proc_macro::TokenStream,
// ) -> proc_macro::TokenStream {
//     let cloned_input = input.clone();
//     let parsed = syn::parse_macro_input!(cloned_input as syn::DeriveInput);
//     let original: proc_macro2::TokenStream = input.into();

//     let html = syn::parse_macro_input!(attr as dom::KeyedItemTemplate);

//     let context = dom::Context::init();

//     let generated = html.impl_keyed_item(&context, &parsed.ident);
//     println!("Generated====================\n{}", generated);
//     quote::quote! (
//         #original
//         #generated
//     )
//     .into()
// }

#[proc_macro_attribute]
pub fn convert_to_impl_keyed_item(
    _attr: proc_macro::TokenStream,
    input: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let parsed = syn::parse_macro_input!(input as convert_to_impl_keyed_item::ImplKeyedItem);
    let generated_code = parsed.generate();
    // println!("{}", generated_code.to_string());
    generated_code.into()
}
