pub trait ParseExt<'a> {
    fn parse_until_semi(self) -> syn::parse::Result<proc_macro2::TokenStream>;
    fn peek_signal_symbol(self) -> bool;
    fn parse_signal_symbol(self) -> syn::parse::Result<bool>;
    fn parse_constant_symbol(self) -> syn::parse::Result<bool>;
    fn parse_placeholder_symbol(self) -> syn::parse::Result<bool>;
    fn parse_brace<T: syn::parse::Parse>(self) -> syn::parse::Result<T>;
    fn parse_paren<T: syn::parse::Parse>(self) -> syn::parse::Result<T>;
    fn parse_optional_brace<T: syn::parse::Parse>(self) -> syn::parse::Result<Option<T>>;
    fn parse_optional_paren<T: syn::parse::Parse>(self) -> syn::parse::Result<Option<T>>;
    // How to implement this for all custom keywords K?
    // For parsing:
    //      @some_key=some_value
    // fn parse_at_key_value<K, V>(self) -> syn::parse::Result<Option<V>>
    // where
    //     K: syn::parse::Parse,
    //     V: syn::parse::Parse;

    // This is a special case of `parse_at_key_value` (commented out, above)
    // How to generalize this for any custom keywords?
    fn parse_at_component_name<V>(self) -> syn::parse::Result<Option<V>>
    where
        V: syn::parse::Parse;
}

impl<'a> ParseExt<'a> for &'a syn::parse::ParseBuffer<'a> {
    fn parse_until_semi(self) -> syn::parse::Result<proc_macro2::TokenStream> {
        let mut stream = proc_macro2::TokenStream::new();
        loop {
            let tt: proc_macro2::TokenTree = self.parse()?;
            stream.extend(Some(tt));
            if self.peek(syn::token::Semi) {
                let tt: proc_macro2::TokenTree = self.parse()?;
                stream.extend(Some(tt));
                break;
            }
        }
        Ok(stream)
    }

    fn peek_signal_symbol(self) -> bool {
        self.peek(syn::token::Dollar)
    }

    fn parse_signal_symbol(self) -> syn::parse::Result<bool> {
        Ok(self.parse::<Option<syn::token::Dollar>>()?.is_some())
    }

    fn parse_constant_symbol(self) -> syn::parse::Result<bool> {
        Ok(self.parse::<Option<syn::token::Pound>>()?.is_some())
    }

    fn parse_placeholder_symbol(self) -> syn::parse::Result<bool> {
        Ok(self.parse::<Option<syn::token::Underscore>>()?.is_some())
    }

    fn parse_brace<T: syn::parse::Parse>(self) -> syn::parse::Result<T> {
        let brace_content;
        syn::braced!(brace_content in self);
        Ok(brace_content.parse()?)
    }

    fn parse_paren<T: syn::parse::Parse>(self) -> syn::parse::Result<T> {
        let paren_content;
        syn::parenthesized!(paren_content in self);
        Ok(paren_content.parse()?)
    }

    fn parse_optional_brace<T: syn::parse::Parse>(self) -> syn::parse::Result<Option<T>> {
        if self.peek(syn::token::Brace) {
            self.parse_brace().map(Some)
        } else {
            Ok(None)
        }
    }

    fn parse_optional_paren<T: syn::parse::Parse>(self) -> syn::parse::Result<Option<T>> {
        if self.peek(syn::token::Paren) {
            self.parse_paren().map(Some)
        } else {
            Ok(None)
        }
    }

    // fn parse_at_key_value<K, V>(self, f: K) -> syn::parse::Result<Option<V>>
    // where
    //     K: syn::parse::Parse + syn::parse::Peek,
    //     V: syn::parse::Parse,
    // {
    //     if self.peek(syn::token::At) {
    //         if self.peek2(f) {
    //             let _: syn::token::At = self.parse()?;
    //             let _: K = self.parse()?;
    //             let _: syn::token::Eq = self.parse()?;
    //             return Ok(Some(self.parse()?));
    //         }
    //     }
    //     Ok(None)
    //}

    fn parse_at_component_name<V>(self) -> syn::parse::Result<Option<V>>
    where
        V: syn::parse::Parse,
    {
        if self.peek(syn::token::At) && self.peek2(crate::keywords::component_name) {
            let _: syn::token::At = self.parse()?;
            let _: crate::keywords::component_name = self.parse()?;
            let _: syn::token::Eq = self.parse()?;
            return Ok(Some(self.parse()?));
        }
        Ok(None)
    }
}

pub struct IdentGenerator(std::cell::Cell<usize>);

impl IdentGenerator {
    pub fn init() -> Self {
        Self(std::cell::Cell::new(0))
    }
    pub fn ident_from_str(&self, name: &str) -> syn::Ident {
        let counter = self.0.get();
        self.0.set(counter + 1);
        quote::format_ident!("__{}_{}__", name, counter)
    }
}
