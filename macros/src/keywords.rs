syn::custom_keyword!(component_name);
syn::custom_keyword!(render);
syn::custom_keyword!(new);

syn::custom_keyword!(text);
syn::custom_keyword!(const_text);
syn::custom_keyword!(child);
syn::custom_keyword!(child_signal);
syn::custom_keyword!(child_list);
syn::custom_keyword!(list_signal);
syn::custom_keyword!(list_signal_vec);
syn::custom_keyword!(keyed_list_signal_vec);
