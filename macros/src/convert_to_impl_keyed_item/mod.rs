/// This module implement a custom attribute that convert a render method on an item into an `impl mika::KeyedItem<C> for Item`.
/// The implementation is definitely by a noob. You are welcome to re-implement it :D.
use syn::ext::IdentExt;

mod parse;

lazy_static::lazy_static! {
    static ref TEMPLATE_ONLY_METHODS: std::collections::HashSet<&'static str> = {
        [
            "into",
            "class",
            "r#type",
            "template_custom_attribute",
        ].iter().copied().collect()
    };
    static ref RENDER_ONLY_METHODS: std::collections::HashSet<&'static str> = {
        [
            "const_text",
        ].iter().copied().collect()
    };
    static ref METHOD_NAME_MAP: std::collections::HashMap<&'static str, &'static str> = {
        [
            ("input min", "string_min"),
            ("meter min", "f64_min"),
            ("input max", "string_max"),
            ("meter max", "f64_max"),
            ("progress max", "f64_max"),
            // types
            ("button r#type", "button_type"),
            ("input r#type", "input_type"),
            ("a r#type", "mime_type"),
            ("embed r#type", "mime_type"),
            ("object r#type", "mime_type"),
            ("source r#type", "mime_type"),
            ("ol r#type", "ol_type"),
            // values
            ("li value", "li_value"),
            ("meter value", "f64_value"),
            ("progress value", "f64_value"),
            ("button value", "string_value"),
            ("data value", "string_value"),
            ("input value", "string_value"),
            ("option value", "string_value"),
            ("param value", "string_value"),
        ].iter().copied().collect()
    };
}

fn map_name(tag: &str, attribute: &syn::Ident) -> syn::Ident {
    let s = format!("{} {}", tag.to_ascii_lowercase(), attribute);
    //println!("Mapping name for: {}", s);
    match METHOD_NAME_MAP.get(s.as_str()) {
        Some(s) => quote::format_ident!("{}", s, span = attribute.span()),
        None => attribute.clone(),
    }
}

struct Context {
    ident_generator: crate::helper::IdentGenerator,
}
impl Context {
    pub fn init() -> Self {
        Self {
            ident_generator: crate::helper::IdentGenerator::init(),
        }
    }
}

#[derive(Debug)]
pub struct ImplKeyedItem {
    pub impl_token: syn::token::Impl,
    pub item_type: SimplePath,
    pub fn_render: FnRender,
}

#[derive(Debug)]
pub struct FnRender {
    pub fn_token: syn::token::Fn,
    pub fn_name: crate::keywords::render,
    pub args: FnRenderArgs,
    pub return_type: syn::Path,
    pub body: FnRenderBody,
}

#[derive(Debug)]
pub struct FnRenderArgs {
    pub and_for_self: syn::token::And,
    pub self_ident: syn::token::SelfValue,
    pub comp_ident: syn::Ident,
    pub and_for_comp_handle: syn::token::And,
    pub mika_comp_handle_type: SimplePath,
    pub user_component_type: SimplePath,
}

#[derive(Debug)]
pub struct FnRenderBody {
    pub uses: Vec<proc_macro2::TokenStream>,
    pub lets: Vec<proc_macro2::TokenStream>,
    pub element: Element,
}

#[derive(Debug)]
pub struct Element {
    pub need_update: bool,
    pub generated_ident: GeneratedIdent,
    pub element_constructor_call: ElementConstructorCall,
    // pub attribute_method_calls: Vec<AttributeMethodCall>,
    // pub sub_item_method_calls: Vec<SubItemMethodCall>,
    // Back to MethodCall because of ".into()"
    pub method_calls: Vec<MethodCall>,
}

#[derive(Debug)]
pub struct GeneratedIdent(std::cell::RefCell<Option<syn::Ident>>);

impl GeneratedIdent {
    pub fn init() -> Self {
        GeneratedIdent(std::cell::RefCell::new(None))
    }

    fn generate(&self, context: &Context, base: &str) {
        self.0
            .borrow_mut()
            .replace(context.ident_generator.ident_from_str(base));
    }

    fn get(&self) -> syn::Ident {
        self.0
            .borrow()
            .as_ref()
            .expect("Ident not generated yet")
            .clone()
    }
}

#[derive(Debug)]
pub enum MethodCall {
    Attribute(AttributeMethodCall),
    SubItem(SubItemMethodCall),
}

#[derive(Debug)]
pub enum AttributeMethodCall {
    // type, for, class
    TemplateOnly(NameArgs),
    // events, signals...
    RenderOnly {
        must_keep_alive: bool,
        name_args: NameArgs,
    },
    // class_if,
    RenderUpdate(NameArgs),
}

#[derive(Debug)]
pub enum SubItemMethodCall {
    // TODO: Should introducing a `ShareText` (text value that is the same for every item in the list)??
    // A value of an item (different items have different values), that is never change, such as an item's id.
    ConstText(TextMethodCall),
    Text(TextMethodCall),
    Child(ChildMethodCall),
    //ChildSignal(NameArgs),
    //List(NameArgs),
}

#[derive(Debug)]
pub struct TextMethodCall {
    generated_ident: GeneratedIdent,
    name_args: NameArgs,
}

#[derive(Debug)]
pub struct ChildMethodCall {
    name: syn::Ident,
    element: Element,
}

#[derive(Debug)]
pub struct NameArgs {
    name: syn::Ident,
    args: proc_macro2::TokenStream,
}

/// A simple path just contains idents and separators `::`
#[derive(Debug)]
pub struct SimplePath(pub proc_macro2::TokenStream);

impl quote::ToTokens for SimplePath {
    fn to_tokens(&self, output: &mut proc_macro2::TokenStream) {
        output.extend(self.0.clone());
    }
}

#[derive(Debug)]
pub struct ElementConstructorCall {
    // This is a full associated method call like: mika::dom::Div::new()
    pub constructor_call: proc_macro2::TokenStream,
    // Bonus field: Div/Span...
    pub element_type: syn::Ident,
}

impl quote::ToTokens for ElementConstructorCall {
    fn to_tokens(&self, output: &mut proc_macro2::TokenStream) {
        output.extend(self.constructor_call.clone());
    }
}

impl ImplKeyedItem {
    pub fn generate(&self) -> proc_macro2::TokenStream {
        let item_type = &self.item_type;
        let items_need_update_type_name = quote::format_ident!(
            "ItemsNeedUpdateOf{}",
            item_type.0.to_string().replace(" ", "").replace("::", "")
        );
        let comp_ident = &self.fn_render.args.comp_ident;
        let mika_comp_handle_type = &self.fn_render.args.mika_comp_handle_type;
        let user_component_type = &self.fn_render.args.user_component_type;
        let output_type = &self.fn_render.return_type;

        let (items_need_update_type, items_need_update_value) = self
            .fn_render
            .body
            .element
            .start_generate_items_need_update(&items_need_update_type_name);

        let fn_template = self.fn_render.body.generate_fn_template();
        let fn_render = self.fn_render.body.generate_fn_render(
            comp_ident,
            mika_comp_handle_type,
            user_component_type,
            &items_need_update_value,
        );

        let fn_update = self.fn_render.body.generate_fn_update(
            comp_ident,
            mika_comp_handle_type,
            user_component_type,
        );
        quote::quote! {
            #items_need_update_type
            impl mika::KeyedItem<#user_component_type> for #item_type {
                type RenderOutput = #output_type;
                type ItemsNeedUpdate = #items_need_update_type_name;
                #fn_template
                #fn_render
                #fn_update
            }
        }
    }
}

impl FnRenderBody {
    fn generate_fn_template(&self) -> proc_macro2::TokenStream {
        let uses = &self.uses;
        let template_code = self.element.generate_template();
        quote::quote! {
            fn template() -> Self::RenderOutput {
                #(#uses)*
                #template_code
            }
        }
    }

    fn generate_fn_render(
        &self,
        comp_ident: &syn::Ident,
        mika_comp_handle_type: &SimplePath,
        user_component_type: &SimplePath,
        items_need_update_value: &proc_macro2::TokenStream,
    ) -> proc_macro2::TokenStream {
        let template_ident = quote::format_ident!("__fn_render_template_arg_ident_");
        let total_element_ident =
            quote::format_ident!("__the_borrow_mut_total_element_of_the_template_arg_");
        let uses = &self.uses;
        let lets = &self.lets;
        let ws_ident = quote::format_ident!("__template_ws_node_in_keyed_item_render_");
        let ws_nodes_code = self.element.start_generate_ws_node_for_render(&ws_ident);
        let render_code = self.element.start_generate_render(&total_element_ident);
        quote::quote! {
            fn render(&self,
                      #comp_ident: &#mika_comp_handle_type<#user_component_type>,
                      mut #template_ident: Self::RenderOutput
            ) -> (Self::RenderOutput, Self::ItemsNeedUpdate) {
                #![allow(unused_variables)]

                use wasm_bindgen::UnwrapThrowExt;
                use mika::prelude::HasWsNode;
                #(#uses)*
                #(#lets)*
                let #ws_ident = #template_ident.ws_node().clone();
                #ws_nodes_code

                let #total_element_ident = #template_ident.total_element_mut();
                #render_code
                // Restore the original node
                #total_element_ident.replace_ws_element(#ws_ident.as_ref());


                (#template_ident, #items_need_update_value)
            }
        }
    }

    fn generate_fn_update(
        &self,
        comp_ident: &syn::Ident,
        mika_comp_handle_type: &SimplePath,
        user_component_type: &SimplePath,
    ) -> proc_macro2::TokenStream {
        let uses = &self.uses;
        let lets = &self.lets;
        let element = quote::format_ident!("__fn_update_element_arg_ident_");
        let total_element =
            quote::format_ident!("__the_borrow_mut_total_element_of_the_element_arg_");
        let items = quote::format_ident!("__fn_update_items_arg_ident_");
        let update_code = self.element.start_generate_update(&total_element, &items);
        quote::quote! {
            fn update(&self,
                      #comp_ident: &#mika_comp_handle_type<#user_component_type>,
                      #element: &mut Self::RenderOutput,
                      #items: &mut Self::ItemsNeedUpdate,
            ) {
                #![allow(unused_variables)]

                use wasm_bindgen::UnwrapThrowExt;
                #(#uses)*
                #(#lets)*

                let #total_element = #element.total_element_mut();
                let __ws_node_in_keyed_item_update_ = #total_element.ws_node().clone();

                #update_code

                #total_element.replace_ws_element(__ws_node_in_keyed_item_update_.as_ref());
            }
        }
    }
}

enum WsNode {
    Parent(syn::Ident),
    PrevSibling(syn::Ident),
}

impl Element {
    fn start_generate_items_need_update(
        &self,
        type_name: &syn::Ident,
    ) -> (proc_macro2::TokenStream, proc_macro2::TokenStream) {
        let name = format!(
            "template_{}",
            self.element_constructor_call
                .element_type
                .to_string()
                .to_ascii_lowercase()
        );
        let context = Context::init();
        let mut items_need_update_type = Vec::new();
        let mut items_need_update_value = Vec::new();
        self.method_calls.iter().for_each(|s| {
            s.generate_items_need_update(
                &context,
                &name,
                &mut items_need_update_type,
                &mut items_need_update_value,
            )
        });
        (
            quote::quote! {
                pub struct #type_name {
                    #(#items_need_update_type),*
                }
            },
            quote::quote! {
                #type_name{
                    #(#items_need_update_value),*
                }
            },
        )
    }

    fn generate_items_need_update(
        &self,
        context: &Context,
        items_need_update_type: &mut Vec<proc_macro2::TokenStream>,
        items_need_update_value: &mut Vec<syn::Ident>,
    ) {
        let tag = self
            .element_constructor_call
            .element_type
            .to_string()
            .to_ascii_lowercase();
        self.generated_ident
            .generate(context, &format!("ws_node_{}", tag));
        if self.need_update {
            let ident = self.generated_ident.get();
            items_need_update_type.push(quote::quote!(#ident: mika::WsNode));
            items_need_update_value.push(ident);
        }
        self.method_calls.iter().for_each(|s| {
            s.generate_items_need_update(
                context,
                &tag,
                items_need_update_type,
                items_need_update_value,
            )
        });
    }

    fn generate_template(&self) -> proc_macro2::TokenStream {
        let constructor_call = &self.element_constructor_call;
        let method_calls = self.method_calls.iter().map(|c| c.generate_template());
        quote::quote! {
            #constructor_call
            #(#method_calls)*
        }
    }

    fn start_generate_ws_node_for_render(&self, ws_node: &syn::Ident) -> proc_macro2::TokenStream {
        let mut ws_node = WsNode::Parent(ws_node.clone());
        let method_calls = self
            .method_calls
            .iter()
            .map(|c| c.generate_ws_node_for_render(&mut ws_node));
        quote::quote! {
            #(#method_calls)*
        }
    }

    fn generate_ws_node_for_render(&self, ws_node: &mut WsNode) -> proc_macro2::TokenStream {
        let element_ident = self.generated_ident.get();
        let get_ws_node = match ws_node {
            WsNode::Parent(parent) => {
                quote::quote! {
                    let #element_ident = #parent.first_child().unwrap_throw();
                }
            }
            WsNode::PrevSibling(previous_sibling) => {
                quote::quote! {
                    let #element_ident = #previous_sibling.next_sibling().unwrap_throw();
                }
            }
        };

        *ws_node = WsNode::PrevSibling(element_ident.clone());

        let mut sub_ws_node = WsNode::Parent(element_ident.clone());
        let method_calls = self
            .method_calls
            .iter()
            .map(|c| c.generate_ws_node_for_render(&mut sub_ws_node));
        quote::quote! {
            #get_ws_node
            #(#method_calls)*
        }
    }

    fn start_generate_render(&self, template_ident: &syn::Ident) -> proc_macro2::TokenStream {
        let attribute_method_calls = self.generate_render_attributes(template_ident);
        let sub_item_method_calls = self.generate_render_sub_items(template_ident);
        quote::quote! {
            #attribute_method_calls
            #sub_item_method_calls
        }
    }

    fn generate_render(&self, template_ident: &syn::Ident) -> proc_macro2::TokenStream {
        let ident = self.generated_ident.get();
        let render_code = self.start_generate_render(template_ident);
        quote::quote! {
            // TODO: only generate this line when the node need to render (attributes)
            #template_ident.replace_ws_element(&#ident);

            #render_code
        }
    }

    fn generate_render_attributes(&self, template_ident: &syn::Ident) -> proc_macro2::TokenStream {
        let mut stream = proc_macro2::TokenStream::new();
        self.method_calls.iter().for_each(|c| match c {
            MethodCall::Attribute(a) => stream.extend(a.generate_render(
                &self.element_constructor_call.element_type.to_string(),
                &template_ident,
            )),
            MethodCall::SubItem(_) => {}
        });
        stream
    }

    fn generate_render_sub_items(&self, template_ident: &syn::Ident) -> proc_macro2::TokenStream {
        let mut stream = proc_macro2::TokenStream::new();
        self.method_calls.iter().for_each(|c| match c {
            MethodCall::Attribute(_) => {}
            MethodCall::SubItem(s) => stream.extend(s.generate_render(template_ident)),
        });
        stream
    }

    fn start_generate_update(
        &self,
        element: &syn::Ident,
        items: &syn::Ident,
    ) -> proc_macro2::TokenStream {
        let update_root_attributes = self.generate_update_attributes(element);
        let update_sub_items = self.generate_update_sub_items(element, items);
        quote::quote! {
            #update_root_attributes
            #update_sub_items
        }
    }

    fn generate_update(
        &self,
        element: &syn::Ident,
        items: &syn::Ident,
    ) -> proc_macro2::TokenStream {
        let update_attributes = if self.need_update {
            let ident = self.generated_ident.get();
            let update_attributes = self.generate_update_attributes(element);
            quote::quote! {
                #element.replace_ws_element(&#items.#ident);
                #update_attributes
            }
        } else {
            proc_macro2::TokenStream::new()
        };
        let update_sub_items = self.generate_update_sub_items(element, items);
        quote::quote! {
            #update_attributes
            #update_sub_items
        }
    }

    fn generate_update_attributes(&self, element: &syn::Ident) -> proc_macro2::TokenStream {
        let mut stream = proc_macro2::TokenStream::new();
        self.method_calls.iter().for_each(|c| match c {
            MethodCall::Attribute(a) => stream.extend(a.generate_update(
                &self.element_constructor_call.element_type.to_string(),
                &element,
            )),
            MethodCall::SubItem(_) => {}
        });
        stream
    }

    fn generate_update_sub_items(
        &self,
        element: &syn::Ident,
        items: &syn::Ident,
    ) -> proc_macro2::TokenStream {
        let mut stream = proc_macro2::TokenStream::new();
        self.method_calls.iter().for_each(|c| match c {
            MethodCall::Attribute(_) => {}
            MethodCall::SubItem(s) => stream.extend(s.generate_update(
                &self.element_constructor_call.element_type.to_string(),
                element,
                items,
            )),
        });
        stream
    }
}

impl MethodCall {
    fn generate_items_need_update(
        &self,
        context: &Context,
        tag: &str,
        items_need_update_type: &mut Vec<proc_macro2::TokenStream>,
        items_need_update_value: &mut Vec<syn::Ident>,
    ) {
        match self {
            MethodCall::Attribute(_a) => {}
            MethodCall::SubItem(s) => s.generate_items_need_update(
                context,
                tag,
                items_need_update_type,
                items_need_update_value,
            ),
        }
    }
    fn generate_template(&self) -> proc_macro2::TokenStream {
        match self {
            MethodCall::Attribute(a) => a.generate_template(),
            MethodCall::SubItem(c) => c.generate_template(),
        }
    }

    fn generate_ws_node_for_render(&self, ws_node: &mut WsNode) -> proc_macro2::TokenStream {
        match self {
            MethodCall::Attribute(_) => proc_macro2::TokenStream::new(),
            MethodCall::SubItem(c) => c.generate_ws_node_for_render(ws_node),
        }
    }
}

impl AttributeMethodCall {
    fn generate_template(&self) -> proc_macro2::TokenStream {
        use AttributeMethodCall::*;
        match self {
            TemplateOnly(na) => na.generate_chained_call(),
            RenderOnly { .. } | RenderUpdate(_) => proc_macro2::TokenStream::new(),
        }
    }

    fn generate_render(&self, tag: &str, ident: &syn::Ident) -> proc_macro2::TokenStream {
        use AttributeMethodCall::*;
        match self {
            TemplateOnly(_) => proc_macro2::TokenStream::new(),
            RenderOnly { name_args, .. } => name_args.generate_update_call(tag, ident),
            RenderUpdate(na) => na.generate_update_call(tag, ident),
        }
    }

    fn generate_update(&self, tag: &str, ident: &syn::Ident) -> proc_macro2::TokenStream {
        use AttributeMethodCall::*;
        match self {
            TemplateOnly(_) | RenderOnly { .. } => proc_macro2::TokenStream::new(),
            RenderUpdate(na) => na.generate_update_call(tag, ident),
        }
    }
}

impl SubItemMethodCall {
    fn generate_items_need_update(
        &self,
        context: &Context,
        parent_tag: &str,
        items_need_update_type: &mut Vec<proc_macro2::TokenStream>,
        items_need_update_value: &mut Vec<syn::Ident>,
    ) {
        use SubItemMethodCall::*;
        match self {
            Child(c) => c.element.generate_items_need_update(
                context,
                items_need_update_type,
                items_need_update_value,
            ),
            Text(t) => t.generate_items_need_update(
                context,
                parent_tag,
                items_need_update_type,
                items_need_update_value,
            ),
            ConstText(_t) => {}
        }
    }

    fn generate_template(&self) -> proc_macro2::TokenStream {
        use SubItemMethodCall::*;
        match self {
            Child(c) => c.generate_template(),
            Text(t) => t.generate_template(),
            ConstText(t) => t.generate_template(),
        }
    }

    fn generate_ws_node_for_render(&self, ws_node: &mut WsNode) -> proc_macro2::TokenStream {
        use SubItemMethodCall::*;
        match self {
            Child(c) => c.element.generate_ws_node_for_render(ws_node),
            Text(t) => t.generate_ws_node_for_render(ws_node),
            ConstText(t) => t.generate_ws_node_for_render(ws_node),
        }
    }

    fn generate_render(&self, template_ident: &syn::Ident) -> proc_macro2::TokenStream {
        use SubItemMethodCall::*;
        match self {
            Child(c) => c.element.generate_render(template_ident),
            Text(t) => t.generate_render(),
            ConstText(t) => t.generate_set_text_content(),
        }
    }

    fn generate_update(
        &self,
        tag: &str,
        element: &syn::Ident,
        items: &syn::Ident,
    ) -> proc_macro2::TokenStream {
        use SubItemMethodCall::*;
        match self {
            Child(c) => c.element.generate_update(element, items),
            Text(t) => t.generate_update(tag, items),
            ConstText(_) => proc_macro2::TokenStream::new(),
        }
    }
}

impl TextMethodCall {
    fn generate_items_need_update(
        &self,
        context: &Context,
        parent_tag: &str,
        items_need_update_type: &mut Vec<proc_macro2::TokenStream>,
        items_need_update_value: &mut Vec<syn::Ident>,
    ) {
        self.generated_ident
            .generate(context, &format!("text_node_of_{}", parent_tag));
        let ident = self.generated_ident.get();
        items_need_update_type.push(quote::quote!(#ident: mika::dom::Text));
        items_need_update_value.push(ident);
    }

    fn generate_template(&self) -> proc_macro2::TokenStream {
        quote::quote! {
            .text("?")
        }
    }

    fn generate_ws_node_for_render(&self, ws_node: &mut WsNode) -> proc_macro2::TokenStream {
        let text_ident = self.generated_ident.get();
        let output = match ws_node {
            WsNode::Parent(ref parent) => {
                quote::quote! {
                    let #text_ident = #parent.first_child().unwrap_throw();
                }
            }
            WsNode::PrevSibling(ref previous_sibling) => {
                quote::quote! {
                    let #text_ident = #previous_sibling.next_sibling().unwrap_throw();
                }
            }
        };
        *ws_node = WsNode::PrevSibling(text_ident.clone());
        output
    }

    fn generate_render(&self) -> proc_macro2::TokenStream {
        let ident = self.generated_ident.get();
        let text_value = &self.name_args.args;
        quote::quote! {
            let #ident = mika::dom::Text::with_node_and_text(#ident, #text_value);
        }
    }

    fn generate_set_text_content(&self) -> proc_macro2::TokenStream {
        let ident = self.generated_ident.get();
        let text_value = &self.name_args.args;
        quote::quote! {
            #ident.set_text_content(Some(#text_value));
        }
    }

    fn generate_update(&self, tag: &str, items: &syn::Ident) -> proc_macro2::TokenStream {
        let ident = self.generated_ident.get();
        let update_text = self.name_args.generate_update_call(tag, &ident);
        quote::quote! {
            #items.#update_text
        }
    }
}

impl ChildMethodCall {
    fn generate_template(&self) -> proc_macro2::TokenStream {
        let name = &self.name;
        let template_code = &self.element.generate_template();
        quote::quote! {
            .#name(#template_code)
        }
    }
}

impl NameArgs {
    fn generate_chained_call(&self) -> proc_macro2::TokenStream {
        let name = &self.name;
        let args = &self.args;
        quote::quote! {
            .#name(#args)
        }
    }

    fn generate_update_call(&self, tag: &str, ident: &syn::Ident) -> proc_macro2::TokenStream {
        let args = &self.args;
        // let name = &self.name;
        let name = map_name(tag, &self.name);
        quote::quote! {
            #ident.#name(#args);
        }
    }
}
